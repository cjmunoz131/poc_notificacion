package pe.com.interbank.pys.notificacion.microservices.jolt;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.JsonUtils;

import pe.com.interbank.pys.trace.microservices.exceptions.MicroserviceException;

@Component
public class JoltRequestConversor {

	@Autowired
	private JoltRequestSorter sorter;

	@Autowired
	@Qualifier("conversorList")
	private Map<String, List<Object>> conversorList;

	private String obtenerRequest(String request, String serviceId) {
		List<Object> specs = obtenerSpecs(serviceId);
		Chainr chainr = Chainr.fromSpec(specs);
		Object transformedOutput = chainr.transform(JsonUtils.jsonToObject(request));
		return JsonUtils.toJsonString(transformedOutput);
	}

	public void setSorter(JoltRequestSorter sorter) {
		this.sorter = sorter;
	}
	
	private List<Object> obtenerSpecs(String serviceId) {
		return conversorList.get(serviceId);
	}

	public String obtenerRequest_(String request, String serviceId) throws MicroserviceException{
		return sorter.ordenarRequest(obtenerRequest(request,serviceId),serviceId);
	}
	
	public String obtenerResponse_(String response, String serviceId) throws MicroserviceException {
		return obtenerRequest(response,serviceId);
	}
}

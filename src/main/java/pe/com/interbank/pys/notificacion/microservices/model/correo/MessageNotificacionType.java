package pe.com.interbank.pys.notificacion.microservices.model.correo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageNotificacionType {

	private MessageNotificacion  messageNotificacions;	
		
	@JsonCreator
	public MessageNotificacionType(
			@JsonProperty("message") MessageNotificacion messageNotificacions) {
	
		this.messageNotificacions = messageNotificacions;
	}
	
	@JsonProperty("message")
	public MessageNotificacion getMessageNotificacions() {
		return messageNotificacions;
	}
	public void setMessageNotificacions(MessageNotificacion messageNotificacions) {
		this.messageNotificacions = messageNotificacions;
	}
	
	
	
}

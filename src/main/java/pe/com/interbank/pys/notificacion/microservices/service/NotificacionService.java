package pe.com.interbank.pys.notificacion.microservices.service;

import pe.com.interbank.pys.notificacion.microservices.model.correo.NotificacionResponse;
import pe.com.interbank.pys.notificacion.microservices.model.sms.NotificarResponse;
import pe.com.interbank.pys.trace.microservices.exceptions.MicroserviceException;

public interface NotificacionService {

  /**
   * Funcionalidad de Notificacion medio correo electronico
   * 
   * @param request
   * @return
   */
  public NotificacionResponse notificacionCorreo(String request, long offset) throws MicroserviceException;

  /**
   * Funcionalidad de Notificacion medio correo electronico versión Adjuntos
   * 
   * @param request
   * @return
   */
  public NotificacionResponse notificacionCorreoAdjunto(String request, long offset) throws MicroserviceException;

  /**
   * Funcionalidad que maneja Notificacion correo Error
   * 
   * @param request
   * @return
   */
  public NotificacionResponse notificacionCorreoError(String request, long offset) throws MicroserviceException;

  /**
   * Funcionalidad de Notificacion medio SMS
   * 
   * @param request
   * @return
   */
  public NotificarResponse notificacionSMS(String request, long offset, String messageId) throws MicroserviceException;
  
}

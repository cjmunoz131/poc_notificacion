package pe.com.interbank.pys.notificacion.microservices.model.correo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NotificacionRequestType {

	private String identity;
	private String credential;
	private List<MessageNotificacionType> messageNotificacionType;
	//private ArchivoAdjunto archivoAdjunto;
	
	@JsonCreator
	public NotificacionRequestType(
			@JsonProperty("identity") String identity, 
			@JsonProperty("credential") String credential, 
			@JsonProperty("messages") List<MessageNotificacionType> messageNotificacionType) {
		this.identity = identity;
		this.credential = credential;
		this.messageNotificacionType = messageNotificacionType;
	}
	/*
	@JsonCreator
	public NotificacionRequestType(
			@JsonProperty("identity") String identity, 
			@JsonProperty("credential") String credential, 
			@JsonProperty("messages") List<MessageNotificacionType> messageNotificacionType,
			@JsonProperty("archivoAdjunto") ArchivoAdjunto archivoAdjunto) {
		
		this.identity = identity;
		this.credential = credential;
		this.messageNotificacionType = messageNotificacionType;
		this.archivoAdjunto = archivoAdjunto;
	}
	*/
	public String getIdentity() {
		return identity;
	}
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	public String getCredential() {
		return credential;
	}
	public void setCredential(String credential) {
		this.credential = credential;
	}
	@JsonProperty("messages")
	public List<MessageNotificacionType> getMessageNotificacionType() {
		return messageNotificacionType;
	}
	public void setMessageNotificacionType(List<MessageNotificacionType> messageNotificacionType) {
		this.messageNotificacionType = messageNotificacionType;
	}		
	/*
	@JsonProperty("archivoAdjunto")
	public ArchivoAdjunto getAdjuntoRequestType() {
		return archivoAdjunto;
	}

	public void setAdjuntoRequestType(ArchivoAdjunto archivoAdjunto) {
		this.archivoAdjunto = archivoAdjunto;
	}
*/
	
}

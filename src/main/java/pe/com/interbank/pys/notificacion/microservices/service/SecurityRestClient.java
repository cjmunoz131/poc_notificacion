package pe.com.interbank.pys.notificacion.microservices.service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import pe.com.interbank.pys.notificacion.microservices.util.Constantes;

/**
 * Clase que genera el cliente Rest y maneja SSL
 * 
 * @author Alex Aguirre
 *
 */
@Component
public class SecurityRestClient {

	@Autowired
	@Qualifier("clientBus")
	private Client clientBus;

	@Autowired
	@Qualifier("clientMs")
	private Client clientMs;
	
	private static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json; charset=UTF-8";

	private static final Logger logger = LoggerFactory.getLogger(SecurityRestClient.class);

	public Builder init(String host, String path, String messageId, long offset, boolean isMicroservice) {
		try {
			WebTarget target;
			if (isMicroservice) {
				target = clientMs.target(host).path(path);
			} else {
				target = clientBus.target(host).path(path);
			}
			return target.request(MediaType.APPLICATION_JSON_TYPE);
		} catch (Exception e) {
			logger.error("Excepcion el la inicializacion de SecurityRestClient " + e.getMessage()
					+ Constantes.MESSAGE_ID_LOG + messageId + Constantes.OFFSET_LOG + offset, e);
		}
		return null;
	}

	public Response invokeExternalService(String request, String host, String path, String messageId, long offset,
			String serviceId, String destinationId, boolean isMicroservice) {
		Builder restClient = init(host, path, messageId, offset, isMicroservice);
		return restClient.post(Entity.entity(request, APPLICATION_JSON_CHARSET_UTF_8));
	}

	public void setClientBus(Client clientBus) {
		this.clientBus = clientBus;
	}

	public void setClientMs(Client clientMs) {
		this.clientMs = clientMs;
	}

}

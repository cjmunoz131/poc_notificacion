package pe.com.interbank.pys.notificacion.microservices.model.correo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ParameterType {

	Parameter parameter;
	
	@JsonCreator
	public ParameterType(@JsonProperty("parameter") Parameter parameter) {
		this.parameter = parameter;
	}

	public Parameter getParameter() {
		return parameter;
	}

	public void setParameter(Parameter parameter) {
		this.parameter = parameter;
	}
	
	
	
}

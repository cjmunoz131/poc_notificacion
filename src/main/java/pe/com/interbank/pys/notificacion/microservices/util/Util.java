package pe.com.interbank.pys.notificacion.microservices.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;


public class Util {

	private static final Logger logger = LoggerFactory.getLogger(Util.class);
	private static ObjectMapper objectMapper = new ObjectMapper();
	
	public static String normalizarRequestSMS(String requestEnvParan) {
		JsonNode rootNode=null ;
		try {
			rootNode = objectMapper.readTree(requestEnvParan);	
			String nroCelular = rootNode.path("nroCelular").asText();
			// Tratamiento del celular
			if (nroCelular != null) {
				String nro = nroCelular.replaceAll("[\\D]", "");
				if ( nro.length() >  Integer.parseInt(PropertiesCache.getInstance().getProperty(ConfigConstantes.NOTIFICACION_SMS_PHONE_DIGQTY)))
					nro = nro.substring(nro.length() - Integer.parseInt(PropertiesCache.getInstance().getProperty(ConfigConstantes.NOTIFICACION_SMS_PHONE_DIGQTY)));
				((ObjectNode)rootNode).put("nroCelular", PropertiesCache.getInstance().getProperty(ConfigConstantes.NOTIFICACION_SMS_PHONE_PREFIX)+nro);
			}
		} catch (IOException e) {
			logger.error("Error",e);
		}
		return (rootNode!=null)?rootNode.toString():null;
	}
	
	public static JsonNode getNodeReference(String nombre, JsonNode rootNode) {
		List<JsonNode> padres = rootNode.findParents(nombre);
		if (padres != null && !padres.isEmpty()) {
			Iterator<JsonNode> iterator = padres.iterator();
			while (iterator.hasNext()) {
				JsonNode parent = iterator.next();
				JsonNode campo = parent.get(nombre);
				if (campo != null && !(campo instanceof MissingNode) && !(campo instanceof NullNode)) {
					return campo;
				}
			}
		}
		return null;
	}

}

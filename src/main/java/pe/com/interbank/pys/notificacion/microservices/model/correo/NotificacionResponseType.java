package pe.com.interbank.pys.notificacion.microservices.model.correo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NotificacionResponseType {

	private MessageNotificacionResponseType messageNotificacionResponseType;
	
	@JsonCreator
	public NotificacionResponseType(@JsonProperty("createMessagesResult") MessageNotificacionResponseType messageNotificacionResponseType) {
		this.messageNotificacionResponseType = messageNotificacionResponseType;
	}

	@JsonProperty("createMessagesResult")
	public MessageNotificacionResponseType getMessageNotificacionResponseType() {
		return messageNotificacionResponseType;
	}

	public void setMessageNotificacionResponseType(MessageNotificacionResponseType messageNotificacionResponseType) {
		this.messageNotificacionResponseType = messageNotificacionResponseType;
	}
	
	
	
}

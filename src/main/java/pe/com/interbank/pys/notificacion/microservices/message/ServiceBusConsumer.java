package pe.com.interbank.pys.notificacion.microservices.message;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.microsoft.azure.servicebus.ExceptionPhase;
import com.microsoft.azure.servicebus.IMessage;
import com.microsoft.azure.servicebus.IMessageHandler;
import com.microsoft.azure.servicebus.MessageHandlerOptions;
import com.microsoft.azure.servicebus.SubscriptionClient;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;

@Component
public class ServiceBusConsumer {
	
	private SubscriptionClient subscriptionClient;
	
	@Autowired
	public ServiceBusConsumer(SubscriptionClient subscriptionClient) {
		this.subscriptionClient = subscriptionClient;
		this.registerMessageHandleOnClient();
	}
	
	private void registerMessageHandleOnClient(){
		try {
		IMessageHandler messageHandler = new IMessageHandler() {

			@Override
			public CompletableFuture<Void> onMessageAsync(IMessage message) {
				// TODO Auto-generated method stub
				if (message.getLabel() != null &&
                        message.getContentType() != null &&
                        message.getLabel().contentEquals("notificacion") &&
                        message.getContentType().contentEquals("application/json")) {

                    byte[] body = message.getBody();
                    String bodyJson =  new String(body, StandardCharsets.UTF_8);
                    Map notificacion = new Gson().fromJson(bodyJson, Map.class);

                    System.out.printf(
                            "\n\t\t\t\t%s Message received: \n\t\t\t\t\t\tMessageId = %s, \n\t\t\t\t\t\tSequenceNumber = %s, \n\t\t\t\t\t\tEnqueuedTimeUtc = %s," +
                                    "\n\t\t\t\t\t\tExpiresAtUtc = %s, \n\t\t\t\t\t\tContentType = \"%s\",  \n\t\t\t\t\t\tContent: [ body = %s ]\n",
                                    subscriptionClient.getEntityPath(),
                            message.getMessageId(),
                            message.getSequenceNumber(),
                            message.getEnqueuedTimeUtc(),
                            message.getExpiresAtUtc(),
                            message.getContentType(),
                            notificacion != null ? notificacion : "");
                }
                return subscriptionClient.completeAsync(message.getLockToken());
			}

			@Override
			public void notifyException(Throwable exception, ExceptionPhase phase) {
				// TODO Auto-generated method stub
				System.out.println(phase + exception.getMessage());
				exception.printStackTrace();
			}
			
		};
		ExecutorService exservice = Executors.newFixedThreadPool(5);
			this.subscriptionClient.registerMessageHandler(messageHandler,
					new MessageHandlerOptions(1, false, Duration.ofMinutes(1)), exservice);
		}catch(ServiceBusException sbe) {
			sbe.printStackTrace();
		}catch(InterruptedException ie) {
			ie.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}

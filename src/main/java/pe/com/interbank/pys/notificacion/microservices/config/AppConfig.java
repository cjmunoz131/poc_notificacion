package pe.com.interbank.pys.notificacion.microservices.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import javax.ws.rs.client.Client;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.microsoft.azure.servicebus.ReceiveMode;
import com.microsoft.azure.servicebus.SubscriptionClient;
import com.microsoft.azure.servicebus.primitives.ConnectionStringBuilder;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;

import pe.com.interbank.pys.notificacion.microservices.jolt.FileProcessor;
import pe.com.interbank.pys.notificacion.microservices.message.ConsumerListener;
import pe.com.interbank.pys.notificacion.microservices.service.RouterServiceImpl;
import pe.com.interbank.pys.notificacion.microservices.util.ConfigConstantes;
import pe.com.interbank.pys.trace.microservices.azure.KafkaAuthenticationAzure;
import pe.com.interbank.pys.trace.microservices.azure.ScheduledListenerAzure;
import pe.com.interbank.pys.trace.microservices.config.RestClientConfig;
import pe.com.interbank.pys.trace.microservices.message.BaseConsumerListener;
import pe.com.interbank.pys.trace.microservices.message.ProducerCallback;
import pe.com.interbank.pys.trace.microservices.message.ServiceConsumer;
import pe.com.interbank.pys.trace.microservices.service.AbstractKafkaSenderService;
import pe.com.interbank.pys.trace.microservices.service.AuditoriaService;
import pe.com.interbank.pys.trace.microservices.service.AuditoriaServiceImpl;
import pe.com.interbank.pys.trace.microservices.service.KafkaSenderServiceAsyncImpl;
import pe.com.interbank.pys.trace.microservices.service.MultipleAsyncMService;
import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;

@Configuration
public class AppConfig {

	@Bean
	public KafkaProducer<Integer, String> producerFactory() {
		return KafkaAuthenticationAzure.getProducer();
	}

	@Bean
	public ProducerCallback producerCallback() {
		return new ProducerCallback();
	}

	@Bean(name = "clientMs")
	Client msClient() {
		return RestClientConfig.restClientInitAzure();
	}

	@Bean(name = "clientBus")
	Client busClient() {
		return RestClientConfig.restClientInitAzure();
	}

	@Bean
	public AuditoriaService auditoriaService() {
		return new AuditoriaServiceImpl();
	}

	@Bean
	@Qualifier("conversorList")
	public Map<String, List<Object>> conversorList() {
		HashMap<String, List<Object>> specList = new HashMap<>();
		FileProcessor.readConversors(specList);
		return specList;
	}

	@Bean
	@Qualifier("sorterList")
	public Map<String, List<Object>> sorterList() {
		HashMap<String, List<Object>> specList = new HashMap<>();
		FileProcessor.readSorters(specList);
		return specList;
	}

	@Bean(name = "auditoriaThreadPoolTaskExecutor")
	public Executor auditoriaThreadPoolTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(
				Integer.parseInt(PropertiesCache.getInstance().getProperty(ConfigConstantes.AUDITORIA_MIN_THREADS)));
		executor.setMaxPoolSize(
				Integer.parseInt(PropertiesCache.getInstance().getProperty(ConfigConstantes.AUDITORIA_MAX_THREADS)));
		executor.setQueueCapacity(
				Integer.parseInt(PropertiesCache.getInstance().getProperty(ConfigConstantes.AUDITORIA_QUEUE_SIZE)));
		executor.setThreadNamePrefix("AuditoriaAsync-");
		executor.initialize();
		return executor;
	}
	
	@Bean(name = "asyncThreadPoolExecutor")
	@Qualifier("asyncThreadPoolExecutor")
	public Executor asyncThreadPoolExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(
				Integer.parseInt(PropertiesCache.getInstance().getProperty(ConfigConstantes.EXECUTION_MIN_THREADS)));
		executor.setMaxPoolSize(
				Integer.parseInt(PropertiesCache.getInstance().getProperty(ConfigConstantes.EXECUTION_MAX_THREADS)));
		executor.setQueueCapacity(
				Integer.parseInt(PropertiesCache.getInstance().getProperty(ConfigConstantes.EXECUTION_QUEUE_SIZE)));
		executor.setThreadNamePrefix("ExecutionAsync-");
		executor.initialize();
		return executor;
	}

	@Bean
	@Qualifier("routerService")
	public MultipleAsyncMService routerService() {
		return new RouterServiceImpl();
	}

	@Bean
	BaseConsumerListener consumerListener(@Qualifier("routerService") MultipleAsyncMService service) {
		return new ConsumerListener(service);
	}

	@Bean
	ServiceConsumer serviceConsumer() {
		return new ServiceConsumer();
	}

	@Bean
	ScheduledListenerAzure scheduledListener() {
		return new ScheduledListenerAzure(PropertiesCache.getInstance().getProperty(ConfigConstantes.TOPICO_ASINCRONO),
				PropertiesCache.getInstance().getProperty(ConfigConstantes.TOPICO_RECOVER));
	}
	
	@Bean
	public SubscriptionClient subscriptionClient() throws InterruptedException, ServiceBusException {
		return new SubscriptionClient(
				new ConnectionStringBuilder(PropertiesCache.getInstance().getProperty(ConfigConstantes.CONNECTION_SB),
						PropertiesCache.getInstance().getProperty(ConfigConstantes.TOPIC_ASINCRONO_SB)
								+ "/subscriptions/"
								+ PropertiesCache.getInstance()
										.getProperty(ConfigConstantes.NOTIFICATION_SUBSCRIPTION)),
				ReceiveMode.PEEKLOCK);
	}

	@Bean
	public AbstractKafkaSenderService kafkaAsyncService() {
		return new KafkaSenderServiceAsyncImpl();
	}
}

package pe.com.interbank.pys.notificacion.microservices.model.correo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NotificacionResponse {

	private MessageResponse messageResponse;
	
	
	public NotificacionResponse(@JsonProperty("MessageResponse") MessageResponse messageResponse) {
		
		this.messageResponse = messageResponse;
	}

	public MessageResponse getMessageResponse() {
		return messageResponse;
	}

	public void setMessageResponse(MessageResponse messageResponse) {
		this.messageResponse = messageResponse;
	}
	
	
	
	
}

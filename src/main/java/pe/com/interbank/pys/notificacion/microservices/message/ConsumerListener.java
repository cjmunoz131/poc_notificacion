package pe.com.interbank.pys.notificacion.microservices.message;

import javax.inject.Inject;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.interbank.pys.notificacion.microservices.util.ConfigConstantes;
import pe.com.interbank.pys.notificacion.microservices.util.Constantes;
import pe.com.interbank.pys.trace.microservices.message.BaseConsumerListener;
import pe.com.interbank.pys.trace.microservices.service.MultipleAsyncMService;
import pe.com.interbank.pys.trace.microservices.util.Encryptor;
import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;

/**
 * Listener que escucha el topico kafka y procesa los mensajes
 * 
 * @author Gustavo Choque
 * @author fpinedaa
 *
 */
public class ConsumerListener implements BaseConsumerListener {

	private static final Logger logger = LoggerFactory.getLogger(ConsumerListener.class);

	private ObjectMapper objectMapper = new ObjectMapper();

	private MultipleAsyncMService routerService;

	@Inject
	public ConsumerListener(MultipleAsyncMService routerService) {
		this.routerService = routerService;
	}

	public void onMessage(ConsumerRecord<Integer, String> message) {
		try {
			String request = Encryptor.decrypt(PropertiesCache.getInstance().getProperty(ConfigConstantes.AES_KEY),
					PropertiesCache.getInstance().getProperty(ConfigConstantes.AES_VECTOR), message.value());
			JsonNode rootNode = objectMapper.readTree(request);
			JsonNode operation = rootNode.path(Constantes.OPERATION);
			if (PropertiesCache.getInstance().getProperty(ConfigConstantes.TOPICO_ASINCRONO).equals(message.topic())
					&& routerService.getServiceIdList().contains(operation.asText())) {
				routerService.onMessage(request, message.offset(), operation.asText());
			} else if (PropertiesCache.getInstance().getProperty(ConfigConstantes.TOPICO_RECOVER)
					.equals(message.topic())) {
				routerService.onMessage(request, message.offset(), Constantes.NOTIFICACION_RECOVERY);
			}
		} catch (Exception e) {
			logger.error("Error en el Microservicio:" + e.getMessage() + Constantes.OFFSET_LOG + message.offset()
					+ " topico: " + message.topic(), e);
		}
	}

	public void setService(MultipleAsyncMService routerService) {
		this.routerService = routerService;
	}
}

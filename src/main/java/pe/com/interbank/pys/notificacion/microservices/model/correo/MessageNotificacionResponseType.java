package pe.com.interbank.pys.notificacion.microservices.model.correo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageNotificacionResponseType {

	private List<MessageNotificacionResponse> messageNotificacionResponse;

	@JsonCreator
	public MessageNotificacionResponseType(
			@JsonProperty("data") List<MessageNotificacionResponse> messageNotificacionResponse) {
		this.messageNotificacionResponse = messageNotificacionResponse;
	}

	@JsonProperty("data")
	public List<MessageNotificacionResponse> getMessageNotificacionResponse() {
		return messageNotificacionResponse;
	}

	public void setMessageNotificacionResponse(List<MessageNotificacionResponse> messageNotificacionResponse) {
		this.messageNotificacionResponse = messageNotificacionResponse;
	}

}

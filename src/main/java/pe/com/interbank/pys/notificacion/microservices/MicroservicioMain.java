package pe.com.interbank.pys.notificacion.microservices;

import pe.com.interbank.pys.notificacion.microservices.server.NotificacionServer;

public class MicroservicioMain {
	
	protected MicroservicioMain() {
		throw new IllegalAccessError("Clase de ejecución");
	}

	public static void main(String[] args) {
		NotificacionServer.main(args);
	}
}

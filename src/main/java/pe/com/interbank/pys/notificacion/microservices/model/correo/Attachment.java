package pe.com.interbank.pys.notificacion.microservices.model.correo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Attachment {

	private String attachment;
	
	@JsonCreator
	public Attachment(@JsonProperty("attachment") String attachment) {
		this.attachment = attachment;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	
	
	
	
	
}

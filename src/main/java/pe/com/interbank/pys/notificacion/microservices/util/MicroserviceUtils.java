package pe.com.interbank.pys.notificacion.microservices.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import pe.com.interbank.pys.trace.microservices.util.JsonUtil;
import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;

public class MicroserviceUtils {

	private static final Logger logger = LoggerFactory.getLogger(MicroserviceUtils.class);
	private static final String ERROR_LOG = "Error en el microservicio : ";
	private static final String HEADER = "Header";

	protected MicroserviceUtils() {
		throw new IllegalAccessError("Clase estatica");
	}

	public static String registrarTimeOut(String requestEnv, long offset) {
		String messageId = JsonUtil.getCampoTrama(Constantes.MESSAGE_ID_MAP,requestEnv);
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode rootNodeEnv = objectMapper.readTree(requestEnv);
			JsonNode rootNodeError = objectMapper
					.readTree(PropertiesCache.getInstance().getProperty(ConfigConstantes.BODY_ERROR));
			JsonNode requestHeader = rootNodeEnv.path("MessageRequest").path(HEADER).path("HeaderRequest")
					.path("request");
			JsonNode messageResponse = rootNodeError.path("MessageResponse").path(HEADER).path("HeaderResponse");
			((ObjectNode) messageResponse).set("response", requestHeader);

			return rootNodeError.toString();

		} catch (JsonProcessingException e) {
			logger.error("Error al construir el objeto Json de TimeOut" + e.getMessage() + Constantes.MESSAGE_ID_LOG
					+ messageId + Constantes.OFFSET_LOG + offset, e);
		} catch (Exception e) {
			logger.error("Error en registrarTimeOut:" + e.getMessage() + Constantes.MESSAGE_ID_LOG + messageId
					+ Constantes.OFFSET_LOG + offset, e);
		}
		return "Error al registrar timeout" + Constantes.MESSAGE_ID_LOG + messageId + Constantes.OFFSET_LOG + offset;
	}

	public static JsonNode obtenerNode(String response, String msCode, String msMessage) {
		JsonNode responseNode = null;
		try {
			responseNode = new ObjectMapper().readTree(response);
			JsonNode statusNode = responseNode.path("MessageResponse").path("Header").path("HeaderResponse")
					.path("status");
			if (statusNode != null && !(statusNode instanceof MissingNode)) {
				((ObjectNode) statusNode).put("msResponseCode", msCode);
				((ObjectNode) statusNode).put("msResponseMessage", msMessage);
			}
		} catch (IOException e) {
			String mensaje = ERROR_LOG + e.getMessage();
			logger.error(mensaje, e);
		}

		return responseNode;
	}

}

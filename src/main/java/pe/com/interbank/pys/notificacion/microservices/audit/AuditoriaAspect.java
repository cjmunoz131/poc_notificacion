package pe.com.interbank.pys.notificacion.microservices.audit;

import javax.ws.rs.core.Response;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;

import pe.com.interbank.pys.notificacion.microservices.util.Constantes;
import pe.com.interbank.pys.notificacion.microservices.util.MicroserviceUtils;
import pe.com.interbank.pys.trace.microservices.service.AuditoriaService;
import pe.com.interbank.pys.trace.microservices.util.JsonUtil;

@Aspect
@Component
public class AuditoriaAspect {

	private static final String ERROR_CODE = "-1";
	public static final String ERROR_INVOCAR_BUS = "Error al invocar el Servicio Bus ";

	@Autowired
	private AuditoriaService auditoriaService;

	@Before("execution(* pe.com.interbank.pys.*.microservices.service.SecurityRestClient.invokeExternalService(..))")
	public void auditoriaRequest(JoinPoint joinPoint) {
		Object[] parametros = joinPoint.getArgs();
		String request = JsonUtil.getTrama(parametros[0]);
		String path = (String) parametros[2];
		long offset = (long) parametros[4];
		String serviceId = (String) parametros[5];
		String destinationServiceId = (String) parametros[6];
		String messageId = JsonUtil.getCampoTrama(Constantes.MESSAGE_ID_MAP, request);
		auditoriaService.escribirAuditoria(messageId, request, Constantes.MSG_TYPE_REQUEST, serviceId, path, offset,
				destinationServiceId);

	}

	@AfterReturning(pointcut = "execution(* pe.com.interbank.pys.*.microservices.service.SecurityRestClient.invokeExternalService(..))", returning = "resultado")
	public void auditoriaResponse(JoinPoint joinPoint, Object resultado) {
		Response response = (Response) resultado;
		response.bufferEntity();
		Object[] parametros = joinPoint.getArgs();
		String request = JsonUtil.getTrama(parametros[0]);
		String responseService = response.readEntity(String.class);
		String path = (String) parametros[2];
		long offset = (long) parametros[4];
		String serviceId = (String) parametros[5];
		String destinationServiceId = (String) parametros[6];
		String messageId = JsonUtil.getCampoTrama(Constantes.MESSAGE_ID_MAP, request);
		auditoriaService.escribirAuditoria(messageId, responseService, Constantes.MSG_TYPE_RESPONSE, serviceId, path,
				offset, destinationServiceId);
	}

	@AfterThrowing(pointcut = "execution(* pe.com.interbank.pys.*.microservices.service.SecurityRestClient.invokeExternalService(..))", throwing = "error")
	public void auditoriaError(JoinPoint joinPoint, Throwable error) {
		Object[] parametros = joinPoint.getArgs();
		String request = (String) parametros[0];
		String path = (String) parametros[2];
		long offset = (long) parametros[4];
		String serviceId = (String) parametros[5];
		String destinationServiceId = (String) parametros[6];
		String responseService = MicroserviceUtils.registrarTimeOut(request, offset);
		String statusMsCode = ERROR_CODE;
		String statusMsMessage = ERROR_INVOCAR_BUS + path + " Error: " + error.getMessage();
		JsonNode responseNode = MicroserviceUtils.obtenerNode(responseService, statusMsCode, statusMsMessage);
		String messageId = JsonUtil.getCampoTrama(Constantes.MESSAGE_ID_MAP, request);
		auditoriaService.escribirAuditoria(messageId, responseNode.toString(), Constantes.MSG_TYPE_RESPONSE, serviceId,	
				path, offset, destinationServiceId);
	}

	public void setAuditoriaService(AuditoriaService auditoriaService) {
		this.auditoriaService = auditoriaService;
	}

}

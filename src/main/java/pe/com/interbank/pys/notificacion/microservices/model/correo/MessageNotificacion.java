package pe.com.interbank.pys.notificacion.microservices.model.correo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageNotificacion {

	private String id;
	private String from;
	private String destination;
	private String cc;
	private String cco;
	private String type;
	private String templateName;
	private String tags;
	private String carrier;
	private List<Attachment> attachments;
	private String appSource;
	private String emailRespuesta;
	private List<ParameterType> parameters;

	@JsonCreator
	public MessageNotificacion(@JsonProperty("id") String id, @JsonProperty("from") String from,
			@JsonProperty("destination") String destination, @JsonProperty("cc") String cc,
			@JsonProperty("cco") String cco, @JsonProperty("emailRespuesta") String emailRespuesta,
			@JsonProperty("type") String type, @JsonProperty("templateName") String templateName,
			@JsonProperty("tags") String tags, @JsonProperty("carrier") String carrier,
			@JsonProperty("attachments") List<Attachment> attachments, @JsonProperty("appSource") String appSource,
			@JsonProperty("parameters") List<ParameterType> parameters
			) {

		this.id = id;
		this.from = from;
		this.destination = destination;
		this.cc  =cc;
		this.cco = cco;
		this.type = type;
		this.templateName = templateName;
		this.tags = tags;
		this.carrier = carrier;
		this.attachments = attachments;
		this.appSource = appSource;
		this.parameters = parameters;
		this.emailRespuesta = emailRespuesta;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	@JsonProperty("attachments")
	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public String getAppSource() {
		return appSource;
	}

	public void setAppSource(String appSource) {
		this.appSource = appSource;
	}

	public List<ParameterType> getParameters() {
		return parameters;
	}

	public void setParameters(List<ParameterType> parameters) {
		this.parameters = parameters;
	}

	public String getEmailRespuesta() {
		return emailRespuesta;
	}

	public void setEmailRespuesta(String emailRespuesta) {
		this.emailRespuesta = emailRespuesta;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getCco() {
		return cco;
	}

	public void setCco(String cco) {
		this.cco = cco;
	}		
	
}

package pe.com.interbank.pys.notificacion.microservices.util;

/**
 * Constantes de Configuracion del Microservicio
 * 
 * @author Adrian Pareja
 *
 */
public class ConfigConstantes {

	
	//Correo C/S Adjunto
	public static final String SERVICE_ID = "service.id";
	public static final String SERVICE_NOTIFICACION = "service.id";
	public static final String SERVICE_NAME = "service.name";
	public static final String SERVICE_GENERARARCHIVO = "service.generararchivo";
	public static final String SERVICE_NOTIFICACIONADJUNTO = "service.notificacion.adjunto";

	//SMS
	public static final String SERVICE_ID_NOTIFICAR = "service.id.notificar";

	public static final String NOTIFICACION_SMS_PHONE_DIGQTY = "enviosms.phone.digqty";
	public static final String NOTIFICACION_SMS_PHONE_PREFIX = "enviosms.phone.prefix";
	public static final String CONVERSOR_NOTIFICAR_SMS_RESPONSE = "notificar.sms.response";
	
	public static final String TOPICO_LOG = "topico.log";
	public static final String TOPICO_RECOVER = "topico.recover";
	public static final String TOPICO_ASINCRONO = "topico.asincrono";
	public static final String TOPICO_PRIMER_RETRY = "topico.primerretry";

	public static final String MESSAGEHUB_BROKERS = "messagehub.brokers";
	public static final String MESSAGEHUB_KEY = "messagehub.key";

	public static final String REST_HOST = "rest.host";
	public static final String REST_FWK4_HOST = "rest.fwk4.host";
	
	public static final String REST_FWK4_SMS_PATH = "rest.fwk4.path.sms";
	
	public static final String REST_FWK4_NOTIFICACION_PATH = "rest.fwk4.path.correo";
	public static final String REST_GENERARARCHIVO_HOST = "rest.generararchivo.host";
	public static final String REST_GENERARARCHIVO_PATH = "rest.generararchivo.path";

	public static final String SERVICE_CONNECT_TIMEOUT = "service.connect.timeout";
	public static final String SERVICE_READ_TIMEOUT = "service.read.timeout";
	
	// Autenticacion servicios bus
	public static final String REST_USUARIO = "rest.usuario";
	public static final String REST_KEY = "rest.password";

	// Ubicacion de recursos de la aplicacion
	public static final String APP_RESOURCES_LOCATION = "app.resources.location";
	public static final String APP_VOLUMEN_LOCATION = "app.volumen.location";
	public static final String APP_MICROSERVICE_LOCATION = "app.microservice.location";

	// Encriptacion AES
	public static final String AES_KEY = "aes.key";
	public static final String AES_VECTOR = "aes.initvector";

	// Valores Properties de Productor Kafka
	public static final String SEND_TIMEOUT_MS = "send.timeout.ms";

	// Key Store Secure Gateway
	public static final String KEYSTORE_FILE = "keystore.file";
	public static final String KEYSTORE_TYPE = "keystore.type";
	public static final String KEYSTORE_VALUE = "keystore.value";

	// response Error
	public static final String BODY_ERROR = "body.error";
	public static final String ERROR_TIPO = "error.microservice.tipo";
	
	 // Valores Configuracion Cliente Rest
    public static final String CONNECTION_MAX_SIZE = "connection.max.size";
    public static final String CONNECTION_MAX_PER_ROUTE = "connection.max.per.route";

    // Valores Configuracion Server Customizer
    public static final String AUDITORIA_MAX_THREADS = "audit.max.threads";
    public static final String AUDITORIA_MIN_THREADS = "audit.min.threads";
    public static final String AUDITORIA_QUEUE_SIZE = "audit.queue.size";
    public static final String HEADER_RQ_TIMESTAMP_FORMATO = "asincrono.header.request.timestamp.format";
	
    public static final String CONVERSORS_MS_PATH = "conversors.ms.path";
	public static final String SORTERS_MS_PATH = "sorters.ms.path";
	public static final String NOTIFICACION_RESPONSE = "notificacion.response";
	
	// Execution Threads Configuration
    public static final String EXECUTION_MAX_THREADS = "execution.max.threads";
    public static final String EXECUTION_MIN_THREADS = "execution.min.threads";
    public static final String EXECUTION_QUEUE_SIZE = "execution.queue.size";
    
    public static final String TOPIC_ASINCRONO_SB = "servicebus.topic.asincrono";
    public static final String CONNECTION_SB = "connection.servicebus.athentication";
    public static final String NOTIFICATION_SUBSCRIPTION = "notificacion.subscripcion";
    
	protected ConfigConstantes() {
		throw new IllegalAccessError("Clase Constantes");
	}
}

package pe.com.interbank.pys.notificacion.microservices.modifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import pe.com.interbank.pys.notificacion.microservices.util.ConfigConstantes;
import pe.com.interbank.pys.notificacion.microservices.util.Constantes;
import pe.com.interbank.pys.notificacion.microservices.util.Util;
import pe.com.interbank.pys.trace.microservices.exceptions.MicroserviceException;
import pe.com.interbank.pys.trace.microservices.util.JsonUtil;
import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;

public class NotificacionModifier {

	private static ObjectMapper mapper = new ObjectMapper();
	private static final Logger logger = LoggerFactory.getLogger(NotificacionModifier.class);
	
	public static String actualizarResponseNotificacion(String response, String request, String serviceId) throws MicroserviceException {
		String requestModificado = "{}";
		try {
			JsonNode rootNode = mapper.readTree(requestModificado);
			JsonNode responseNode = JsonUtil.getNode(Constantes.MESSAGE_RESPONSE, response);
			JsonNode bodyNode = JsonUtil.getNodeReference("Body", responseNode);
			JsonNode envioCorreoNode = JsonUtil.getNodeReference(Constantes.ENVIOCORREO_RESPONSE, responseNode);
			if (envioCorreoNode instanceof TextNode) {
				((ObjectNode) bodyNode).set(Constantes.ENVIOCORREO_RESPONSE, null);

			}
			JsonNode requestNode = JsonUtil.getNode(Constantes.PATH_MSG_MESSAGE_REQUEST, request);
			((ObjectNode) rootNode).set(Constantes.PATH_MSG_MESSAGE_REQUEST, requestNode);

			((ObjectNode) rootNode).set(Constantes.MESSAGE_RESPONSE, responseNode);

			requestModificado = mapper.writeValueAsString(rootNode);

		} catch (Exception e) {
			logger.error(PropertiesCache.getInstance().getProperty(ConfigConstantes.ERROR_TIPO) + e.getClass().getName()
					+ " en " + e.getStackTrace()[0].getClassName() + ":" + e.getStackTrace()[0].getMethodName() + " - "
					+ e, e);
			throw new MicroserviceException("Error al modificar request para el Conversor de " + serviceId);
		}
		return requestModificado;
	}

	public static String actualizarRequestNotificacion(String request, String serviceId) throws MicroserviceException {
		try {
			JsonNode rootNode = mapper.readTree(request);
			request = mapper.writeValueAsString(rootNode);
		} catch (Exception e) {
			logger.error(PropertiesCache.getInstance().getProperty(ConfigConstantes.ERROR_TIPO) + e.getClass().getName()
					+ " en " + e.getStackTrace()[0].getClassName() + ":" + e.getStackTrace()[0].getMethodName() + " - "
					+ e, e);
			throw new MicroserviceException("Error al modificar request para el Conversor de " + serviceId);
		}
		return request;
	}
	
	public static String actualizarNotificarSmsFw4ResponseToFw3(String request, String responseFw4Bus)
			throws MicroserviceException {

		try {
			JsonNode notificarReqRootNode = mapper.readTree(request);
			JsonNode requestNode = Util.getNodeReference(Constantes.MSG_TYPE_REQUEST, notificarReqRootNode);

			JsonNode notificarResFw4RootNode = mapper.readTree(responseFw4Bus);
			JsonNode headerResponseFromFw4Node = Util.getNodeReference(Constantes.PATH_MSG_HEADER_RESPONSE, notificarResFw4RootNode);
			((ObjectNode) headerResponseFromFw4Node).remove(Constantes.TIMESTAMP_MAP);

			((ObjectNode) headerResponseFromFw4Node).replace(Constantes.MSG_TYPE_RESPONSE, requestNode);
			
			JsonNode bodyNode = Util.getNodeReference(Constantes.PATH_MSG_BODY, notificarResFw4RootNode);
			JsonNode envioMensajeNode = Util.getNodeReference(Constantes.ENVIO_MENSAJE_RESPONSE_NODE, bodyNode);
			if (envioMensajeNode == null)
				((ObjectNode)bodyNode).replace(Constantes.ENVIO_MENSAJE_RESPONSE_NODE,mapper.readTree("{}"));
			
			return mapper.writeValueAsString(notificarResFw4RootNode);
		} catch (Exception e) {
			logger.error(PropertiesCache.getInstance().getProperty(ConfigConstantes.ERROR_TIPO) + e.getClass().getName()
					+ " en " + e.getStackTrace()[0].getClassName() + ":" + e.getStackTrace()[0].getMethodName() + " - "
					+ e, e);
			throw new MicroserviceException("Error al modificar response de notificarsms Fw4 to Fw3");
		}
	}

}

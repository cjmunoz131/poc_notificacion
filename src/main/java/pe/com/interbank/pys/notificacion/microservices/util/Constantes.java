package pe.com.interbank.pys.notificacion.microservices.util;

import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;

/**
 * Clase que contiene las constantes del Microservicio
 * 
 * @author Adrian Pareja
 *
 */
public class Constantes {

	public static final String BEAN_SERVICE_GRABAR_LDPD = "notificacionService";
	public static final String BEAN_PRODUCER = "KafkaProducer<Integer, String>";
	public static final String BEAN_ENVIRONMENT = "environment";

	public static final String PATH_MSG_HEADER = "Header";
	public static final String PATH_MSG_HEADER_REQUEST = "HeaderRequest";
	public static final String PATH_MSG_HEADER_RESPONSE = "HeaderResponse";
	public static final String PATH_MSG_MESSAGE_REQUEST = "MessageRequest";
	public static final String MSG_TYPE_REQUEST = "request";
	public static final String MSG_TYPE_RESPONSE = "response";
	public static final String MESSAGE_REQUEST = "MessageRequest";
	public static final String MESSAGE_RESPONSE = "MessageResponse";
	public static final String PATH_MSG_BODY = "Body";
	public static final String PATH_MSG_ENVIO_CORREO = "envioCorreo";
	public static final String PATH_MSG_ARCHIVO_ADJUNTO = "archivoAdjunto";

	public static final String MSG_ERROR_SERVICE = "Ocurrio un error inesperado";

	public static final String DATE_FORMAT = "dd/MM/YYYY HH:mm:ss";

	public static final String MESSAGE_ID_MAP = "messageId";
	public static final String CONSUMER_ID_MAP = "consumerId";
	public static final String CANAL_ID_MAP = "canal";
	public static final String MESSAGE_ID_LOG = " messageId: ";
	public static final String SERVICE_ID_LOG = " serviceId: ";
	public static final String OFFSET_LOG = " offset: ";
	public static final String TIMESTAMP_MAP = "timestamp";
	public static final String ERROR_LOG = " error: ";
	public static final String OPERATION = "operation";
	public static final String ENVIOCORREO_RESPONSE = "envioCorreoResponse";
	public static final String ENVIO_MENSAJE_RESPONSE_NODE = "envioMensajeResponse";
	
	//Operation Asincronos
	public static final String NOTIFICACION_CORREO_S_ADJUNTO = "notificacion";
	public static final String NOTIFICACION_CORREO_C_ADJUNTO = "notificacionadjunto";
	public static final String NOTIFICACION_RECOVERY = "notificacionrecovery";

	
	public static final String NOTIFICACION_SMS = PropertiesCache.getInstance()
			.getProperty(ConfigConstantes.SERVICE_ID_NOTIFICAR);
	public static final String NOTIFICACION_CORREO = PropertiesCache.getInstance()
			.getProperty(ConfigConstantes.SERVICE_NOTIFICACION);
	public static final String NOTIFICACION_CORREO_ADJUNTO = PropertiesCache.getInstance()
			.getProperty(ConfigConstantes.SERVICE_NOTIFICACIONADJUNTO);
	
	protected Constantes() {
		throw new IllegalAccessError("Clase Constantes");
	}

}

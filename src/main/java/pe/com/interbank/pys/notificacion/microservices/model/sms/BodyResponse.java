package pe.com.interbank.pys.notificacion.microservices.model.sms;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown=true)
public class BodyResponse {
  @JsonProperty("notificarResponse")
  private NotificarResponseType notificarResponse;
  
  @JsonCreator
  public BodyResponse(@JsonProperty("notificarResponse") NotificarResponseType notificarResponse) {
      this.notificarResponse = notificarResponse;
  }
  @JsonProperty("notificarResponse")
  public NotificarResponseType getRegistroResponseType() {
      return notificarResponse;
  }
  @JsonProperty("notificarResponse")
  public void setRegistroResponseType(NotificarResponseType notificarResponse) {
      this.notificarResponse = notificarResponse;
  }
}

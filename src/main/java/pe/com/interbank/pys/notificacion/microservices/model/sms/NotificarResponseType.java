package pe.com.interbank.pys.notificacion.microservices.model.sms;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown=true)
public class NotificarResponseType {
	private Boolean aceptado;
	private String mensaje;

	@JsonCreator
	public NotificarResponseType(@JsonProperty("aceptado") Boolean aceptado,
			@JsonProperty("mensaje") String mensaje) {
		this.aceptado = aceptado;
		this.mensaje = mensaje;
	}

	@JsonProperty("aceptado")
	public Boolean getAceptado() {
		return aceptado;
	}

	public void setAceptado(Boolean aceptado) {
		this.aceptado = aceptado;
	}

	@JsonProperty("mensaje")
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}

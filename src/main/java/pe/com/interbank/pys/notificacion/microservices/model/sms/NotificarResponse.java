package pe.com.interbank.pys.notificacion.microservices.model.sms;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NotificarResponse {
  @JsonProperty("MessageResponse")
  private MessageResponse messageResponse;

  @JsonCreator
  public NotificarResponse(@JsonProperty("MessageResponse") MessageResponse messageResponse) {
      this.messageResponse = messageResponse;
  }
  @JsonProperty("MessageResponse")
  public MessageResponse getMessageResponse() {
      return messageResponse;
  }
  @JsonProperty("MessageResponse")
  public void setMessageResponse(MessageResponse messageResponse) {
      this.messageResponse = messageResponse;
  }
}

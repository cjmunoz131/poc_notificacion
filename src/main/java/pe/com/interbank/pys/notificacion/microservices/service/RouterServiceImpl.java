package pe.com.interbank.pys.notificacion.microservices.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;

import pe.com.interbank.pys.notificacion.microservices.util.Constantes;
import pe.com.interbank.pys.trace.microservices.exceptions.GenericException;
import pe.com.interbank.pys.trace.microservices.service.MultipleAsyncMService;
import pe.com.interbank.pys.trace.microservices.util.JsonUtil;
/**
 * 
 * @author Fernando Pineda
 *
 */
public class RouterServiceImpl implements MultipleAsyncMService {

	private static final Logger logger = LoggerFactory.getLogger(RouterServiceImpl.class);

	private List<String> serviceIdList;
	
	@PostConstruct
	public void init() {
		serviceIdList = new ArrayList<>();
		serviceIdList.add(Constantes.NOTIFICACION_SMS);
		serviceIdList.add(Constantes.NOTIFICACION_CORREO);
		serviceIdList.add(Constantes.NOTIFICACION_CORREO_ADJUNTO);
		serviceIdList.add(Constantes.NOTIFICACION_RECOVERY);
	}

	@Autowired
	private NotificacionService notificacionService;

	@Override
	@Async("asyncThreadPoolExecutor")
	public void onMessage(String request, long offset, String serviceId) throws GenericException {
		String messageId = JsonUtil.getCampoTrama(Constantes.MESSAGE_ID_MAP, request);
		logger.info("Recibiendo mensaje: {}{}{}{}{}{}", Constantes.MESSAGE_ID_LOG, messageId, Constantes.OFFSET_LOG,offset
				,Constantes.SERVICE_ID_LOG, serviceId);
		if (Constantes.NOTIFICACION_CORREO.equalsIgnoreCase(serviceId)) {
			notificacionService.notificacionCorreo(request, offset);
		}
		else if (Constantes.NOTIFICACION_SMS.equalsIgnoreCase(serviceId)) {
			notificacionService.notificacionSMS(request, offset, messageId);
		}
		else if (Constantes.NOTIFICACION_CORREO_ADJUNTO.equalsIgnoreCase(serviceId)) {
			notificacionService.notificacionCorreoAdjunto(request, offset);
		}
		else {
			notificacionService.notificacionCorreoError(request, offset);
		}

	}

	@Override
	public List<String> getServiceIdList() {
		return serviceIdList;
	}

	public void setNotificacionService(NotificacionService notificacionService) {
		this.notificacionService = notificacionService;
	}

}

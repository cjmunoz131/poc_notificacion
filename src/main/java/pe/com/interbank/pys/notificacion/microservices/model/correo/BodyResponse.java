package pe.com.interbank.pys.notificacion.microservices.model.correo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BodyResponse {

	private NotificacionResponseType notificacionResponseType;
	
	@JsonCreator
	public BodyResponse(@JsonProperty("envioCorreoResponse") NotificacionResponseType notificacionResponseType) {
		this.notificacionResponseType = notificacionResponseType;
	}

	@JsonProperty("envioCorreoResponse")
	public NotificacionResponseType getNotificacionResponseType() {
		return notificacionResponseType;
	}

	public void setNotificacionResponseType(NotificacionResponseType notificacionResponseType) {
		this.notificacionResponseType = notificacionResponseType;
	}	
	
}

package pe.com.interbank.pys.notificacion.microservices.message;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.interbank.pys.notificacion.microservices.model.correo.Attachment;
import pe.com.interbank.pys.notificacion.microservices.model.correo.BodyRequest;
import pe.com.interbank.pys.notificacion.microservices.model.correo.HeaderRequest;
import pe.com.interbank.pys.notificacion.microservices.model.correo.HeaderRequestType;
import pe.com.interbank.pys.notificacion.microservices.model.correo.Identity;
import pe.com.interbank.pys.notificacion.microservices.model.correo.MessageNotificacion;
import pe.com.interbank.pys.notificacion.microservices.model.correo.MessageNotificacionType;
import pe.com.interbank.pys.notificacion.microservices.model.correo.MessageRequest;
import pe.com.interbank.pys.notificacion.microservices.model.correo.NotificacionRequest;
import pe.com.interbank.pys.notificacion.microservices.model.correo.NotificacionRequestType;
import pe.com.interbank.pys.notificacion.microservices.model.correo.Parameter;
import pe.com.interbank.pys.notificacion.microservices.model.correo.ParameterType;
import pe.com.interbank.pys.notificacion.microservices.model.correo.Request;
import pe.com.interbank.pys.notificacion.microservices.util.ConfigConstantes;
import pe.com.interbank.pys.notificacion.microservices.util.Constantes;
import pe.com.interbank.pys.trace.microservices.util.JsonUtil;
import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;

public class NotificacionRecovery {

	private ObjectMapper objectMapper = new ObjectMapper();

	public static final String REQUESTHEADER_SERVICEID = "requestheader.serviceid";
	public static final String REQUESTHEADER_CONSUMERID = "requestheader.consumerid";
	public static final String REQUESTHEADER_MODULEID = "requestheader.moduleid";
	public static final String REQUESTHEADER_CHANNELCODE = "requestheader.channelcode";
	public static final String REQUESTHEADER_MESSAGEID = "requestheader.messageid";
	public static final String REQUESTHEADER_GROUPMEMBER = "requestheader.groupmember";
	public static final String REQUESTHEADER_REFERENCENUMBER = "requestheader.referencenumber";

	public static final String IDENTITYHEADER_NETID = "identityheader.netid";
	public static final String IDENTITYHEADER_USERID = "identityheader.userid";
	public static final String IDENTITYHEADER_SUPERVISORID = "identityheader.supervisorid";
	public static final String IDENTITYHEADER_DEVICEID = "identityheader.deviceid";
	public static final String IDENTITYHEADER_SERVERID = "identityheader.serverid";
	public static final String IDENTITYHEADER_BRANCHCODE = "identityheader.branchcode";

	public static final String ENVIOCORREO_IDENTITY = "enviocorreo.identity";
	public static final String ENVIOCORREO_CREDENTIAL = "enviocorreo.credential";

	public static final String ENVIOCORREO_MESSAGE_ID = "envioCorreo.message.id";
	public static final String ENVIOCORREO_MESSAGE_FROM = "envioCorreo.message.from";
	public static final String ENVIOCORREO_MESSAGE_DESTINATION = "envioCorreo.message.destination";
	public static final String ENVIOCORREO_MESSAGE_TYPE = "envioCorreo.message.type";
	public static final String ENVIOCORREO_MESSAGE_TEMPLATENAME = "envioCorreo.message.templatename";

	public static final String ENVIOCORREO_MESSAGE_PARAM_CAMPOADICIONAL = "envioCorreo.message.param.campoadicional";
	public static final String ENVIOCORREO_MESSAGE_PARAM_SERVICEID = "envioCorreo.message.param.serviceid";
	public static final String ENVIOCORREO_MESSAGE_PARAM_IDTRANSACCION = "envioCorreo.message.param.idtransaccion";

	private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

	private static final Logger logger = LoggerFactory.getLogger(NotificacionRecovery.class.getName());

	public NotificacionRequest generarRequestNotificacion(String serviceId, String request, long offset) {
		String messageId = JsonUtil.getCampoTrama(Constantes.MESSAGE_ID_MAP, request);
		logger.info("Notificacion Error - Construyendo Request para: " + serviceId + Constantes.MESSAGE_ID_LOG
				+ messageId + Constantes.OFFSET_LOG + offset);
		NotificacionRequest notificacionRequest = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		String fechaActual = dateFormat.format(Calendar.getInstance().getTime());

		String fechaIncidente = "";
		String appSource = "";
		try {
			JsonNode rootNode = objectMapper.readTree(request);

			JsonNode fechaIn = rootNode.path("MessageRequest").path("Header").path("HeaderRequest").path("request")
					.path("timestamp");

			if (!fechaIn.isMissingNode()) {
				fechaIncidente = fechaIn.asText();
			}

			appSource = JsonUtil.getCampoTrama(Constantes.CONSUMER_ID_MAP, request);
			
			if ("".equals(appSource)){
				appSource = JsonUtil.getCampoTrama(Constantes.CANAL_ID_MAP, request);
			}
			if ("".equals(fechaIncidente)) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(PropertiesCache.getInstance().getProperty(ConfigConstantes.HEADER_RQ_TIMESTAMP_FORMATO));
				Date today = Calendar.getInstance().getTime();        
				fechaIncidente = simpleDateFormat.format(today);
			}
			
			Request requestHeader = new Request(PropertiesCache.getInstance().getProperty(REQUESTHEADER_SERVICEID),
					PropertiesCache.getInstance().getProperty(REQUESTHEADER_CONSUMERID),
					PropertiesCache.getInstance().getProperty(REQUESTHEADER_MODULEID),
					PropertiesCache.getInstance().getProperty(REQUESTHEADER_CHANNELCODE), messageId, fechaActual, "PE",
					PropertiesCache.getInstance().getProperty(REQUESTHEADER_GROUPMEMBER),
					PropertiesCache.getInstance().getProperty(REQUESTHEADER_REFERENCENUMBER));

			Identity identiyHeader = new Identity(PropertiesCache.getInstance().getProperty(IDENTITYHEADER_NETID),
					PropertiesCache.getInstance().getProperty(IDENTITYHEADER_USERID),
					PropertiesCache.getInstance().getProperty(IDENTITYHEADER_SUPERVISORID),
					PropertiesCache.getInstance().getProperty(IDENTITYHEADER_DEVICEID),
					PropertiesCache.getInstance().getProperty(IDENTITYHEADER_SERVERID),
					PropertiesCache.getInstance().getProperty(IDENTITYHEADER_BRANCHCODE));

			HeaderRequestType headerRequestType = new HeaderRequestType(requestHeader, identiyHeader);
			HeaderRequest hr = new HeaderRequest(headerRequestType);

			ArrayList<Attachment> lstAttachment = new ArrayList<>();
			ArrayList<ParameterType> lstParameterType = new ArrayList<>();
			lstParameterType.add(new ParameterType(
					new Parameter(PropertiesCache.getInstance().getProperty(ENVIOCORREO_MESSAGE_PARAM_CAMPOADICIONAL),
							fechaIncidente.replace("T", " "))));

			lstParameterType.add(new ParameterType(new Parameter(
					PropertiesCache.getInstance().getProperty(ENVIOCORREO_MESSAGE_PARAM_SERVICEID), serviceId)));
			lstParameterType.add(new ParameterType(new Parameter(
					PropertiesCache.getInstance().getProperty(ENVIOCORREO_MESSAGE_PARAM_IDTRANSACCION), messageId)));

			MessageNotificacion messageNotificacion = new MessageNotificacion(
					PropertiesCache.getInstance().getProperty(ENVIOCORREO_MESSAGE_ID),
					PropertiesCache.getInstance().getProperty(ENVIOCORREO_MESSAGE_FROM),
					PropertiesCache.getInstance().getProperty(ENVIOCORREO_MESSAGE_DESTINATION), "", "", "",
					PropertiesCache.getInstance().getProperty(ENVIOCORREO_MESSAGE_TYPE),
					PropertiesCache.getInstance().getProperty(ENVIOCORREO_MESSAGE_TEMPLATENAME), "", "", lstAttachment,
					appSource, lstParameterType);

			MessageNotificacionType messageNotificacionType = new MessageNotificacionType(messageNotificacion);

			ArrayList<MessageNotificacionType> messageNotificacionTypes = new ArrayList<>();
			messageNotificacionTypes.add(messageNotificacionType);

			NotificacionRequestType notificacionRequestType = new NotificacionRequestType(
					PropertiesCache.getInstance().getProperty(ENVIOCORREO_IDENTITY),
					PropertiesCache.getInstance().getProperty(ENVIOCORREO_CREDENTIAL), messageNotificacionTypes);

			BodyRequest br = new BodyRequest(notificacionRequestType);
			MessageRequest messageRequest = new MessageRequest(hr, br);
			notificacionRequest = new NotificacionRequest(messageRequest);

			logger.info("Notificacion Error - Request Construido para: " + serviceId + Constantes.MESSAGE_ID_LOG
					+ messageId + Constantes.OFFSET_LOG + offset);

		} catch (JsonProcessingException e) {
			logger.error("El REQUEST no tiene la estructura json valida Error: " + e.getMessage()
					+ Constantes.MESSAGE_ID_LOG + messageId + Constantes.OFFSET_LOG + offset, e);
		} catch (IOException e) {
			logger.error("error en generarRequestNotificacion al generar el request de notificacion error: "
					+ e.getMessage() + Constantes.MESSAGE_ID_LOG + messageId + Constantes.OFFSET_LOG + offset, e);
		}

		return notificacionRequest;
	}

	protected void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

}

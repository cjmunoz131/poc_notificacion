package pe.com.interbank.pys.notificacion.microservices.model.sms;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.interbank.pys.notificacion.microservices.model.HeaderResponseType;

public class MessageResponse {
  @JsonProperty("Header")
  private HeaderResponseType header;
  @JsonProperty("Body")
  private BodyResponse body;

  @JsonCreator
  public MessageResponse(@JsonProperty("Header") HeaderResponseType header, @JsonProperty("Body") BodyResponse body) {
      this.header = header;
      this.body = body;
  }
  @JsonProperty("Header")
  public HeaderResponseType getHeader() {
      return header;
  }

  @JsonProperty("Header")
  public void setHeader(HeaderResponseType header) {
      this.header = header;
  }
  @JsonProperty("Body")
  public BodyResponse getBody() {
      return body;
  }
  @JsonProperty("Body")
  public void setBody(BodyResponse body) {
      this.body = body;
  }
}

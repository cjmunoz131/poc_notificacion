package pe.com.interbank.pys.notificacion.microservices.model.correo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageNotificacionResponse {

	private String id;
	private String externalId;
	private String statusId;
	private String lastChange;

	@JsonCreator
	public MessageNotificacionResponse(@JsonProperty("id") String id, @JsonProperty("external_id") String externalId,
			@JsonProperty("status_id") String statusId, @JsonProperty("last_change") String lastChange) {

		this.id = id;
		this.externalId = externalId;
		this.statusId = statusId;
		this.lastChange = lastChange;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("external_id")
	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	@JsonProperty("status_id")
	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	@JsonProperty("last_change")
	public String getLastChange() {
		return lastChange;
	}

	public void setLastChange(String lastChange) {
		this.lastChange = lastChange;
	}

}

package pe.com.interbank.pys.notificacion.microservices.model.correo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ArchivoAdjunto {

	private String tipoArchivo;
	private String template;
	private String nombreArchivo;
	private String numeroDocumento;
	private List<ParameterType> parameters;
	
	@JsonCreator
	public ArchivoAdjunto(
			@JsonProperty("tipoArchivo") String tipoArchivo, 
			@JsonProperty("template") String template,
			@JsonProperty("nombreArchivo") String nombreArchivo, 
			@JsonProperty("numeroDocumento") String numeroDocumento, 
			@JsonProperty("parameters") List<ParameterType> parameters) {
		this.tipoArchivo = tipoArchivo;
		this.template = template;
		this.nombreArchivo = nombreArchivo;
		this.numeroDocumento = numeroDocumento;
		this.parameters = parameters;
	}

	@JsonProperty("tipoArchivo")
	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	@JsonProperty("template")
	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	@JsonProperty("nombreArchivo")
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	@JsonProperty("numeroDocumento")
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	@JsonProperty("parameters")
	public List<ParameterType> getParameters() {
		return parameters;
	}

	public void setParameters(List<ParameterType> parameters) {
		this.parameters = parameters;
	}
	
	
	
}

package pe.com.interbank.pys.notificacion.microservices.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Clase que realiza la configuracion del contexto spring del microservicio
 * 
 * @author **
 */
@Configuration
@EnableScheduling
@EnableAutoConfiguration
@ComponentScan(basePackages = "pe.com.interbank.pys.notificacion")
@EnableAsync
public class NotificacionServer {

	private static final Logger logger = LoggerFactory.getLogger(NotificacionServer.class.getName());

	/**
	 * Ejecuta la Aplicacion sobre Spring Boot y un contenedor de Servlets
	 * 
	 * @param args
	 *            Argumentos de programa
	 * 
	 */
	public static void main(String[] args) {
		logger.info("Iniciando server Notificacion");
		new SpringApplicationBuilder(NotificacionServer.class).web(false).run(args);
	}

}

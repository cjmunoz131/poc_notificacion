package pe.com.interbank.pys.notificacion.microservices.model.correo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Parameter {

	String key;
	String value;
	
	@JsonCreator
	public Parameter(
			@JsonProperty("key") String key, 
			@JsonProperty("value") String value) {
		
		this.key = key;
		this.value = value;		
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
			
}

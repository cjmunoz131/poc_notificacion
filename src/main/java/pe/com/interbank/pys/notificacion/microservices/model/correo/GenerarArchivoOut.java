package pe.com.interbank.pys.notificacion.microservices.model.correo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GenerarArchivoOut {

	private String nombreArchivo;
	private String rutaArchivo;
	private String tipoArchivo;
	

	 public GenerarArchivoOut(){
		 
	 }
	public GenerarArchivoOut(
			@JsonProperty("nombreArchivo") String nombreArchivo,
			@JsonProperty("rutaArchivo") String rutaArchivo, 
			@JsonProperty("tipoArchivo") String tipoArchivo) {
		this.nombreArchivo = nombreArchivo;
		this.rutaArchivo = rutaArchivo;
		this.tipoArchivo = tipoArchivo;
	}
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getRutaArchivo() {
		return rutaArchivo;
	}
	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}
	public String getTipoArchivo() {
		return tipoArchivo;
	}
	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}



	
}

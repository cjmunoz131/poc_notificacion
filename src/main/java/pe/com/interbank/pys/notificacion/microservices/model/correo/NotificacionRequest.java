package pe.com.interbank.pys.notificacion.microservices.model.correo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NotificacionRequest {

	private MessageRequest messageRequest;
	
	@JsonCreator
	public NotificacionRequest(@JsonProperty("MessageRequest") MessageRequest messageRequest) {
		this.messageRequest = messageRequest;
	}

	@JsonProperty("MessageRequest")
	public MessageRequest getMessageRequest() {
		return messageRequest;
	}

	public void setMessageRequest(MessageRequest messageRequest) {
		this.messageRequest = messageRequest;
	}
	
	
}

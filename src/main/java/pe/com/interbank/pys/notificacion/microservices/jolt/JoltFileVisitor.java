package pe.com.interbank.pys.notificacion.microservices.jolt;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bazaarvoice.jolt.JsonUtils;

import pe.com.interbank.pys.trace.microservices.util.Constantes;

class JoltFileVisitor extends SimpleFileVisitor<Path> {

	private static final Logger logger = LoggerFactory.getLogger(JoltFileVisitor.class);
	private Map<String, List<Object>> specList;

	JoltFileVisitor(Map<String, List<Object>> specList) {
		super();
		this.specList = specList;
	}
	
	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
		if (attr.isRegularFile()) {
			String nombreArchivo = file.getFileName().toString().replace(".json", "");
			specList.put(nombreArchivo, JsonUtils.filepathToList(file.toString()));
		}
		return FileVisitResult.CONTINUE;
	}

	/**
	 * imprime el directorio actual
	 */
	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
		return FileVisitResult.CONTINUE;
	}

	/**
	 * control de errores
	 */
	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) {
		logger.error("Error al recorrer archivos: {} {} {} {} {}", exc.getMessage(), Constantes.MESSAGE_ID_LOG, "JoltFileVisitor",Constantes.OFFSET_LOG, -10L, exc);
		return FileVisitResult.CONTINUE;
	}

}

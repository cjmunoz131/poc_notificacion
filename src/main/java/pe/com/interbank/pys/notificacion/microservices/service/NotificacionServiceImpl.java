package pe.com.interbank.pys.notificacion.microservices.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import pe.com.interbank.pys.notificacion.microservices.jolt.JoltRequestConversor;
import pe.com.interbank.pys.notificacion.microservices.message.NotificacionRecovery;
import pe.com.interbank.pys.notificacion.microservices.model.correo.Attachment;
import pe.com.interbank.pys.notificacion.microservices.model.correo.BodyResponse;
import pe.com.interbank.pys.notificacion.microservices.model.correo.GenerarArchivoOut;
import pe.com.interbank.pys.notificacion.microservices.model.correo.MessageNotificacion;
import pe.com.interbank.pys.notificacion.microservices.model.correo.MessageNotificacionResponse;
import pe.com.interbank.pys.notificacion.microservices.model.correo.MessageNotificacionType;
import pe.com.interbank.pys.notificacion.microservices.model.correo.NotificacionRequest;
import pe.com.interbank.pys.notificacion.microservices.model.correo.NotificacionResponse;
import pe.com.interbank.pys.notificacion.microservices.model.correo.NotificacionResponseType;
import pe.com.interbank.pys.notificacion.microservices.model.sms.MessageResponse;
import pe.com.interbank.pys.notificacion.microservices.model.sms.NotificarResponse;
import pe.com.interbank.pys.notificacion.microservices.modifier.NotificacionModifier;
import pe.com.interbank.pys.notificacion.microservices.util.ConfigConstantes;
import pe.com.interbank.pys.notificacion.microservices.util.Constantes;
import pe.com.interbank.pys.notificacion.microservices.util.Util;
import pe.com.interbank.pys.trace.microservices.exceptions.GenericException;
import pe.com.interbank.pys.trace.microservices.exceptions.LegacyException;
import pe.com.interbank.pys.trace.microservices.exceptions.MicroserviceException;
import pe.com.interbank.pys.trace.microservices.service.AbstractKafkaSenderService;
import pe.com.interbank.pys.trace.microservices.util.JsonUtil;
import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;

@Service
public class NotificacionServiceImpl implements NotificacionService {

	private static final Logger logger = LoggerFactory.getLogger(NotificacionServiceImpl.class);

	@Autowired
	JoltRequestConversor joltRequestConversor;
	
	@Autowired
	private SecurityRestClient securityRestClient;

	@Autowired
	private AbstractKafkaSenderService kafkaSender;
	
	private ObjectMapper objectMapper = new ObjectMapper();
	private static final String TAG_REINTENTOS = "numeroReintentos";
	private static final String TAG_OPERACION = "operation";

	public NotificacionServiceImpl() {
		super();
	}

	@Override
	public NotificacionResponse notificacionCorreo(String requestEnv, long offset) throws MicroserviceException {
		return procesaNotificacionRequest(requestEnv, offset, false);
	}

	@Override
	public NotificacionResponse notificacionCorreoAdjunto(String requestEnv, long offset) throws MicroserviceException {
		return procesaNotificacionRequest(requestEnv, offset, true);
	}

	@Override
	public NotificacionResponse notificacionCorreoError(String request, long offset) throws MicroserviceException {
		String messageId = JsonUtil.getCampoTrama(Constantes.MESSAGE_ID_MAP, request);
		try {
			JsonNode rootNode = objectMapper.readTree(request);
			JsonNode operation = rootNode.path(TAG_OPERACION);
			NotificacionRecovery notificacionRecovery = new NotificacionRecovery();
			NotificacionRequest notificacionRequest = notificacionRecovery
					.generarRequestNotificacion(operation.asText(), request, offset);
			String requestString = objectMapper.writeValueAsString(notificacionRequest);
			JsonNode rootNodeTemp = objectMapper.readTree(requestString);
			((ObjectNode) rootNodeTemp).put(TAG_OPERACION, "notificacion");
			String requestEnv = objectMapper.writeValueAsString(rootNodeTemp);
			return procesaNotificacionRequest(requestEnv, offset, false);
		} catch (IOException e) {
			logger.error("Error al parsear el Json del request :" + e.getMessage() + Constantes.OFFSET_LOG 
					+ offset + Constantes.MESSAGE_ID_LOG + messageId, e);
		}
		
		return null;
	}

	public NotificacionResponse procesaNotificacionRequest(String requestEnv, long offset, boolean isAdjunto)
			throws MicroserviceException {
		String messageId = JsonUtil.getCampoTrama(Constantes.MESSAGE_ID_MAP, requestEnv);
		try {
			JsonNode rootNode = objectMapper.readTree(requestEnv);
			((ObjectNode) rootNode).remove(TAG_OPERACION);
			((ObjectNode) rootNode).remove(TAG_REINTENTOS);
			String request = objectMapper.writeValueAsString(rootNode);
			String requestGenerarArchivo = "";
			if (isAdjunto) {
				requestGenerarArchivo = quitarEnvioCorreo(request);
				request = quitarAdjunto(request);
			}
						
			// arregla items desordenados que pueden venir de servicios que no
			// usan commons
			NotificacionRequest objRequest = objectMapper.readValue(request, NotificacionRequest.class);
			
			if (isAdjunto) {
				String responseGenerarArchivo = generarArchivo(requestGenerarArchivo, messageId, offset);
				JsonNode nodeResponse = objectMapper.readTree(responseGenerarArchivo);
				JsonNode archivoAdjunto = nodeResponse.findValue("generarArchivoResponse");
				String sArchivoAdjunto = objectMapper.writeValueAsString(archivoAdjunto);
				GenerarArchivoOut generarArchivoOut = objectMapper.readValue(sArchivoAdjunto, GenerarArchivoOut.class);
				request = agregarAdjunto(objRequest, generarArchivoOut);
				objRequest = objectMapper.readValue(request, NotificacionRequest.class);
			}
			request = objectMapper.writeValueAsString(objRequest);
			request = joltRequestConversor.obtenerRequest_(request,PropertiesCache.getInstance().getProperty(ConfigConstantes.SERVICE_ID));
			String serviceId = isAdjunto
					? PropertiesCache.getInstance().getProperty(ConfigConstantes.SERVICE_NOTIFICACIONADJUNTO)
					: PropertiesCache.getInstance().getProperty(ConfigConstantes.SERVICE_NOTIFICACION);
			Response response = securityRestClient.invokeExternalService(
					request, PropertiesCache.getInstance().getProperty(ConfigConstantes.REST_FWK4_HOST), 
					PropertiesCache.getInstance().getProperty(ConfigConstantes.REST_FWK4_NOTIFICACION_PATH),
					messageId, offset, serviceId, null, false);
			String responseStr = response.readEntity(String.class);
			responseStr = joltRequestConversor.obtenerResponse_(
					NotificacionModifier.actualizarResponseNotificacion(responseStr, request, serviceId),
					PropertiesCache.getInstance().getProperty(ConfigConstantes.NOTIFICACION_RESPONSE));

			
			NotificacionResponse responseLegacy = objectMapper.readValue(responseStr, NotificacionResponse.class);
			String responseType = "6";
			if (responseLegacy.getMessageResponse() != null) {
				responseType = responseLegacy.getMessageResponse().getHeader().getHeaderResponse().getStatus()
						.getResponseType();
			}

			if ("0".equalsIgnoreCase(responseType)) {
				procesarMensajesMultiples(responseLegacy.getMessageResponse().getBody(), requestEnv, messageId, offset);
			} else {
				if ("3".equalsIgnoreCase(responseType) || "4".equalsIgnoreCase(responseType)) {
					logger.error("Error en el Servicio Bus Notificacion: code :" + responseType
							+ Constantes.MESSAGE_ID_LOG + messageId + Constantes.OFFSET_LOG + offset);
					kafkaSender.retryAsincrono(requestEnv, messageId, offset);
				}
			}
			return responseLegacy;
		} catch (JsonProcessingException e) {
			String mensaje = "La respuesta del Servicio Bus "
					+ PropertiesCache.getInstance().getProperty(ConfigConstantes.SERVICE_ID)
					+ "no tiene la estructura json valida" + Constantes.MESSAGE_ID_LOG + messageId
					+ Constantes.OFFSET_LOG + offset + "Error: " + e.getMessage();
			logger.error(mensaje, e);
		} catch (LegacyException e) {
			logger.error(e.getMessage() + Constantes.MESSAGE_ID_LOG + messageId
					+ Constantes.OFFSET_LOG + offset, e);
			kafkaSender.retryAsincrono(requestEnv, messageId, offset);
		} catch (Exception e) {
			logger.error("Error no identificado: " + e.getMessage() + Constantes.MESSAGE_ID_LOG + messageId
					+ Constantes.OFFSET_LOG + offset, e);
			kafkaSender.retryAsincrono(requestEnv, messageId, offset);
		}
		return null;
	}

	private void procesarMensajesMultiples(BodyResponse bodyResponse, String originalRequest, String messageId,
			long offset) {
		NotificacionResponseType responseType = bodyResponse.getNotificacionResponseType();
		if (responseType != null && responseType.getMessageNotificacionResponseType() != null) {
			List<MessageNotificacionResponse> listaMensajes = responseType.getMessageNotificacionResponseType()
					.getMessageNotificacionResponse();
			if (listaMensajes != null && !listaMensajes.isEmpty()) {
				procesarListaMensajes(listaMensajes, originalRequest, messageId, offset);
			}
		}
	}

	private void procesarListaMensajes(List<MessageNotificacionResponse> listaMensajes, String originalRequest,
			String messageId, long offset) {
		List<String> idFallidos = obtenerIdFallidos(listaMensajes);
		if (!idFallidos.isEmpty()) {
			prepararReintento(idFallidos, originalRequest, messageId, offset);
		}
	}

	private List<String> obtenerIdFallidos(List<MessageNotificacionResponse> listaMensajes) {
		List<String> idFallidos = new ArrayList<>();
		Iterator<MessageNotificacionResponse> iterator = listaMensajes.iterator();
		while (iterator.hasNext()) {
			MessageNotificacionResponse messageNotificacionResponse = iterator.next();
			if (!"0".equalsIgnoreCase(messageNotificacionResponse.getStatusId())) {
				idFallidos.add(messageNotificacionResponse.getExternalId());
			}
		}
		return idFallidos;
	}

	protected void prepararReintento(List<String> idFallidos, String originalRequest, String messageId, long offset) {
		try {
			JsonNode rootNode = objectMapper.readTree(originalRequest);
			JsonNode operationNode = rootNode.path(TAG_OPERACION);
			JsonNode reintentosNode = rootNode.path(TAG_REINTENTOS);
			((ObjectNode) rootNode).remove(TAG_OPERACION);
			((ObjectNode) rootNode).remove(TAG_REINTENTOS);
			NotificacionRequest notificacionRequest = objectMapper.treeToValue(rootNode, NotificacionRequest.class);
			List<MessageNotificacionType> listaMensajes = notificacionRequest.getMessageRequest().getBody()
					.getNotificacionRequestType().getMessageNotificacionType();
			List<MessageNotificacionType> listaMensajesFallidos = obtenerMensajesFallidos(idFallidos, listaMensajes);
			notificacionRequest.getMessageRequest().getBody().getNotificacionRequestType()
					.setMessageNotificacionType(listaMensajesFallidos);
			JsonNode rootNodeUpdated = objectMapper.valueToTree(notificacionRequest);
			((ObjectNode) rootNodeUpdated).put(TAG_OPERACION, operationNode.asText());
			if (!reintentosNode.isMissingNode()) {
				((ObjectNode) rootNodeUpdated).put(TAG_REINTENTOS, reintentosNode.asInt());
			}
			String requestUpdated = objectMapper.writeValueAsString(rootNodeUpdated);
			kafkaSender.retryAsincrono(requestUpdated, messageId, offset);
		} catch (JsonProcessingException e) {
			logger.error("Error al construir el objeto Json de Reintento " + e.getMessage() + Constantes.MESSAGE_ID_LOG + messageId
					+ Constantes.OFFSET_LOG + offset, e);
		} catch (Exception e) {
			logger.error("Error en prepararReintento: " + e.getMessage() + Constantes.MESSAGE_ID_LOG + messageId
					+ Constantes.OFFSET_LOG + offset, e);
		}
	}

	private List<MessageNotificacionType> obtenerMensajesFallidos(List<String> idFallidos,
			List<MessageNotificacionType> listaMensajes) {
		List<MessageNotificacionType> listaMensajesFallidos = new ArrayList<>();
		StringBuilder mensajeError = new StringBuilder(
				"Error en el Servicio Bus Notificacion: mensajes erroneos con ids: ");
		Iterator<String> iterator = idFallidos.iterator();
		while (iterator.hasNext()) {
			String idErrado = iterator.next();
			mensajeError.append(idErrado);
			MessageNotificacionType item = obtenerMensaje(listaMensajes, idErrado);
			if (item != null) {
				listaMensajesFallidos.add(item);
			}
			if (iterator.hasNext()) {
				mensajeError.append(", ");
			} else {
				mensajeError.append(".");
			}
		}
		return listaMensajesFallidos;
	}

	public MessageNotificacionType obtenerMensaje(List<MessageNotificacionType> listaMensajes, String idMensaje) {
		Iterator<MessageNotificacionType> iterator = listaMensajes.iterator();
		while (iterator.hasNext()) {
			MessageNotificacionType itemErrado = iterator.next();
			if (idMensaje.equals(itemErrado.getMessageNotificacions().getId())) {
				return itemErrado;
			}
		}
		return null;
	}

	private String quitarAdjunto(String request) throws IOException {
		JsonNode rootNode = objectMapper.readTree(request);
		((ObjectNode) rootNode.path(Constantes.PATH_MSG_MESSAGE_REQUEST).path(Constantes.PATH_MSG_BODY)
				.path(Constantes.PATH_MSG_ENVIO_CORREO)).remove(Constantes.PATH_MSG_ARCHIVO_ADJUNTO);
		return objectMapper.writeValueAsString(rootNode);

	}

	private String agregarAdjunto(NotificacionRequest objRequest, GenerarArchivoOut generarArchivoOut)
			throws IOException {
		String nombre = generarArchivoOut.getRutaArchivo() + generarArchivoOut.getNombreArchivo();
		Attachment attachment = new Attachment(nombre);
		List<Attachment> listaAttachment;
		List<MessageNotificacionType> listaMensajes = objRequest.getMessageRequest().getBody()
				.getNotificacionRequestType().getMessageNotificacionType();
		MessageNotificacionType messageType;
		if (listaMensajes != null && !listaMensajes.isEmpty()) {
			messageType = listaMensajes.get(0);
			MessageNotificacion message = messageType.getMessageNotificacions();
			if (message != null) {
				List<Attachment> listado = message.getAttachments();
				listaAttachment = new ArrayList<>();
				if (listado != null) {
					listaAttachment.add(attachment);
					for (Attachment att : listado) {
						listaAttachment.add(att);
					}
				} else {
					listaAttachment.add(attachment);
				}

				objRequest.getMessageRequest().getBody().getNotificacionRequestType().getMessageNotificacionType()
						.get(0).getMessageNotificacions().setAttachments(listaAttachment);
			}
		}

		return objectMapper.writeValueAsString(objRequest);
	}

	private String quitarEnvioCorreo(String request) throws IOException {
		JsonNode rootNode = objectMapper.readTree(request);
		JsonNode archivoAdjuntoNode = ((ObjectNode) rootNode.path(Constantes.PATH_MSG_MESSAGE_REQUEST)
				.path(Constantes.PATH_MSG_BODY).path("envioCorreo")).get("archivoAdjunto");
		((ObjectNode) rootNode.path(Constantes.PATH_MSG_MESSAGE_REQUEST).path(Constantes.PATH_MSG_BODY))
				.remove("envioCorreo");
		((ObjectNode) rootNode.path(Constantes.PATH_MSG_MESSAGE_REQUEST).path(Constantes.PATH_MSG_BODY))
				.set("generarArchivo", archivoAdjuntoNode);
		return objectMapper.writeValueAsString(rootNode);

	}

	public String generarArchivo(String request, String messageId, long offset) throws GenericException {
		String serviceId = PropertiesCache.getInstance().getProperty(ConfigConstantes.SERVICE_NOTIFICACIONADJUNTO);
		String destinationId = PropertiesCache.getInstance().getProperty(ConfigConstantes.SERVICE_GENERARARCHIVO);
		return securityRestClient.invokeExternalService(request, PropertiesCache.getInstance().getProperty(ConfigConstantes.REST_GENERARARCHIVO_HOST),
				PropertiesCache.getInstance().getProperty(ConfigConstantes.REST_GENERARARCHIVO_PATH),
				messageId, offset, serviceId, destinationId, true).readEntity(String.class);
	}

	@Override
	public NotificarResponse notificacionSMS(String request, long offset, String messageId)
			throws MicroserviceException {
		NotificarResponse responseHost = null;
		String ServiceId = PropertiesCache.getInstance().getProperty(ConfigConstantes.SERVICE_ID_NOTIFICAR);
		String requestBus = joltRequestConversor.obtenerRequest_(Util.normalizarRequestSMS(request),ServiceId);
		Response response;
		try {
			response = securityRestClient.invokeExternalService(requestBus,
					PropertiesCache.getInstance().getProperty(ConfigConstantes.REST_FWK4_HOST),
					PropertiesCache.getInstance().getProperty(ConfigConstantes.REST_FWK4_SMS_PATH),
					messageId,offset,ServiceId,null,
					false);
			String responseFw4String = response.readEntity(String.class);
			String resp = joltRequestConversor.obtenerResponse_(NotificacionModifier.actualizarNotificarSmsFw4ResponseToFw3(requestBus, responseFw4String),PropertiesCache.getInstance().getProperty(ConfigConstantes.CONVERSOR_NOTIFICAR_SMS_RESPONSE));
			responseHost = objectMapper.readValue(resp, NotificarResponse.class);
			responseType_(requestBus,messageId,offset,response,responseHost.getMessageResponse());
			return responseHost;
		} catch (JsonProcessingException e) {
			logger.error("La respuesta del Servicio del Bus"+ServiceId+ "no tiene una estructura JSON valida"
					+ Constantes.MESSAGE_ID_LOG + messageId + Constantes.OFFSET_LOG + offset
					+ Constantes.ERROR_LOG + e.getMessage(),e);
			kafkaSender.recoverAsincrono(requestBus, messageId, offset);
		} catch (ProcessingException e) {
			logger.error("Error en la invocacion al Bus :"+e.getMessage()+Constantes.MESSAGE_ID_LOG+messageId+Constantes.OFFSET_LOG+offset,e);
			kafkaSender.retryAsincrono(requestBus, messageId, offset);
		} catch (Exception e) {
			logger.error("Error no identificado : "+e.getMessage()+Constantes.MESSAGE_ID_LOG+messageId+Constantes.OFFSET_LOG+offset,e);
			kafkaSender.recoverAsincrono(requestBus, messageId, offset);
		}
		return responseHost;
	}
	
	private void responseType_(String request, String messageId, Long offset,Response response ,MessageResponse ms){
		if(response.getStatus() != 200) kafkaSender.retryAsincrono(request, messageId, offset);
		if(ms != null)
			if(ms.getHeader().getHeaderResponse().getStatus().getResponseType().matches("3|4")) {
				kafkaSender.retryAsincrono(request,messageId,offset);	
			}
	}


	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	public void setSecurityRestClient(SecurityRestClient securityRestClient) {
		this.securityRestClient = securityRestClient;
	}

	public void setJoltRequestConversor(JoltRequestConversor joltRequestConversor) {
		this.joltRequestConversor = joltRequestConversor;
	}
}

package pe.com.interbank.pys.notificacion.microservices.model.correo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BodyRequest {

	private NotificacionRequestType notificacionRequestType;
	
	//private ArchivoAdjunto archivoAdjunto;

	@JsonCreator
	public BodyRequest(@JsonProperty("envioCorreo") NotificacionRequestType notificacionRequestType) {
		this.notificacionRequestType = notificacionRequestType;
	}


	@JsonProperty("envioCorreo")
	public NotificacionRequestType getNotificacionRequestType() {
		return notificacionRequestType;
	}

	public void setNotificacionRequestType(NotificacionRequestType notificacionRequestType) {
		this.notificacionRequestType = notificacionRequestType;
	}
	
	
	
}

package pe.com.interbank.pys.notificacion.microservices.model.correo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageRequest {

	private HeaderRequest header;
	private BodyRequest body;

	@JsonCreator
	public MessageRequest(@JsonProperty("Header") HeaderRequest header, @JsonProperty("Body") BodyRequest body) {
		this.header = header;
		this.body = body;
	}

	@JsonProperty("Header")
	public HeaderRequest getHeader() {
		return header;
	}

	public void setHeader(HeaderRequest header) {
		this.header = header;
	}

	@JsonProperty("Body")
	public BodyRequest getBody() {
		return body;
	}

	public void setBody(BodyRequest body) {
		this.body = body;
	}
}

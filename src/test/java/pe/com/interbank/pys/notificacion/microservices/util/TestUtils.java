package pe.com.interbank.pys.notificacion.microservices.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.TopicPartition;

public class TestUtils {

	public static List<ConsumerRecord<Integer, String>> simularXMensajesKafkaCorrelativos(int cantidadMensajes,
			int factorIteracion, String topico) {
		List<ConsumerRecord<Integer, String>> lst = new ArrayList<>();

		for (int i = 0; i < cantidadMensajes; i++) {
			ConsumerRecord<Integer, String> message = new ConsumerRecord<Integer, String>(topico, 0,
					i * factorIteracion, 0, "mensaje" + i);
			lst.add(message);
		}
		return lst;
	}

	public static ConsumerRecords<Integer, String> simularConsumerRecordsCorrelativos(int cantidadMensajes,
			int factorIteracion, String topico) {
		Map<TopicPartition, List<ConsumerRecord<Integer, String>>> r = new HashMap<>();
		List<ConsumerRecord<Integer, String>> lst = simularXMensajesKafkaCorrelativos(cantidadMensajes, factorIteracion,
				topico);
		Set<TopicPartition> topicPartitions = new HashSet<>();
		TopicPartition a = new TopicPartition(topico, 0);
		topicPartitions.add(a);
		r.put(a, lst);
		return new ConsumerRecords<>(r);

	}

}

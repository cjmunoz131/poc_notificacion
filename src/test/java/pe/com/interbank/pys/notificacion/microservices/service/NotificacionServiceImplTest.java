package pe.com.interbank.pys.notificacion.microservices.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import pe.com.interbank.pys.notificacion.microservices.audit.AuditoriaAspect;
import pe.com.interbank.pys.notificacion.microservices.server.NotificacionServer;
import pe.com.interbank.pys.notificacion.microservices.util.Constantes;
import pe.com.interbank.pys.trace.microservices.exceptions.MicroserviceException;
import pe.com.interbank.pys.trace.microservices.service.AbstractKafkaSenderService;
import pe.com.interbank.pys.trace.microservices.service.AuditoriaServiceImpl;
import pe.com.interbank.pys.trace.microservices.util.JsonUtil;
import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { NotificacionServer.class })
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class NotificacionServiceImplTest {

	private static String NOTIFICACION_RESPONSE = PropertiesCache.getInstance()
			.getProperty("test.trama.notificacion.response");
	private static String GENERARARCHIVO_RESPONSE = PropertiesCache.getInstance()
			.getProperty("test.trama.archivo.response");
	private static String NOTIFICACION_TIMEOUT = PropertiesCache.getInstance()
			.getProperty("test.trama.notificacion.timeout");

	private String NOTIFICACION_REQUEST = PropertiesCache.getInstance().getProperty("test.trama.notificacion.request");

	@Autowired
	private NotificacionServiceImpl notificacionServiceImpl;

	@Autowired
	private SecurityRestClient restClient;

	@Autowired
	AuditoriaAspect aspect;

	@Autowired
	AbstractKafkaSenderService kafkaSender;
	
	@Test
	public void notificacionadjuntoTest() throws MicroserviceException {
		AuditoriaServiceImpl auditoriaServiceImpl = Mockito.mock(AuditoriaServiceImpl.class);
		aspect.setAuditoriaService(auditoriaServiceImpl);
		// RESPONSE NOTIFICACION
		Client clientBus = Mockito.mock(Client.class);
		WebTarget target = Mockito.mock(WebTarget.class);
		Builder builder = Mockito.mock(Builder.class);
		Response response = Mockito.mock(Response.class);
		Mockito.when(response.getStatus()).thenReturn(200);
		Mockito.when(response.readEntity(String.class)).thenReturn(NOTIFICACION_RESPONSE);
		Mockito.when(builder.post(Mockito.any(Entity.class))).thenReturn(response);
		Mockito.when(target.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builder);
		Mockito.when(target.path(Mockito.anyString())).thenReturn(target);
		Mockito.when(clientBus.target(Mockito.anyString())).thenReturn(target);
		restClient.setClientBus(clientBus);

		// RESPONSE GENERAR ARCHIVO
		Client clientMs = Mockito.mock(Client.class);
		WebTarget targetMs = Mockito.mock(WebTarget.class);
		Builder builderMs = Mockito.mock(Builder.class);
		Response responseMs = Mockito.mock(Response.class);
		Mockito.when(responseMs.getStatus()).thenReturn(200);
		Mockito.when(responseMs.readEntity(String.class)).thenReturn(GENERARARCHIVO_RESPONSE);
		Mockito.when(builderMs.post(Mockito.any(Entity.class))).thenReturn(responseMs);
		Mockito.when(targetMs.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builderMs);
		Mockito.when(targetMs.path(Mockito.anyString())).thenReturn(targetMs);
		Mockito.when(clientMs.target(Mockito.anyString())).thenReturn(targetMs);
		restClient.setClientMs(clientMs);

		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_REQUEST);
		JsonNode envioCorreoNode = JsonUtil.getNodeReference(Constantes.PATH_MSG_ENVIO_CORREO, root);
		((ObjectNode) envioCorreoNode).put("archivoAdjunto", "archivotest");
		notificacionServiceImpl.notificacionCorreoAdjunto(JsonUtil.getTrama(root), 100L);
	}

	@Test
	public void notificacionTest() throws MicroserviceException {
		AuditoriaServiceImpl auditoriaServiceImpl = Mockito.mock(AuditoriaServiceImpl.class);
		aspect.setAuditoriaService(auditoriaServiceImpl);
		Client client = Mockito.mock(Client.class);
		WebTarget target = Mockito.mock(WebTarget.class);
		Builder builder = Mockito.mock(Builder.class);
		Response response = Mockito.mock(Response.class);
		Mockito.when(response.getStatus()).thenReturn(200);
		Mockito.when(response.readEntity(String.class)).thenReturn(NOTIFICACION_RESPONSE);
		Mockito.when(builder.post(Mockito.any(Entity.class))).thenReturn(response);
		Mockito.when(target.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builder);
		Mockito.when(target.path(Mockito.anyString())).thenReturn(target);
		Mockito.when(client.target(Mockito.anyString())).thenReturn(target);
		restClient.setClientBus(client);
		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_REQUEST);
		((ObjectNode) root).put("numeroReintentos", "1");
		notificacionServiceImpl.notificacionCorreo(JsonUtil.getTrama(root), 100L);
	}

	@Test
	public void notificacionadjuntoErrorHttpMsTest() throws MicroserviceException {
		AuditoriaServiceImpl auditoriaServiceImpl = Mockito.mock(AuditoriaServiceImpl.class);
		aspect.setAuditoriaService(auditoriaServiceImpl);
		// RESPONSE NOTIFICACION
		Client clientBus = Mockito.mock(Client.class);
		WebTarget target = Mockito.mock(WebTarget.class);
		Builder builder = Mockito.mock(Builder.class);
		Response response = Mockito.mock(Response.class);
		Mockito.when(response.getStatus()).thenReturn(200);
		Mockito.when(response.readEntity(String.class)).thenReturn(NOTIFICACION_RESPONSE);
		Mockito.when(builder.post(Mockito.any(Entity.class))).thenReturn(response);
		Mockito.when(target.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builder);
		Mockito.when(target.path(Mockito.anyString())).thenReturn(target);
		Mockito.when(clientBus.target(Mockito.anyString())).thenReturn(target);
		restClient.setClientBus(clientBus);

		// RESPONSE GENERAR ARCHIVO
		Client clientMs = Mockito.mock(Client.class);
		WebTarget targetMs = Mockito.mock(WebTarget.class);
		Builder builderMs = Mockito.mock(Builder.class);
		Response responseMs = Mockito.mock(Response.class);
		Mockito.when(responseMs.getStatus()).thenReturn(500);
		Mockito.when(responseMs.readEntity(String.class)).thenReturn(GENERARARCHIVO_RESPONSE);
		Mockito.when(builderMs.post(Mockito.any(Entity.class))).thenReturn(responseMs);
		Mockito.when(targetMs.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builderMs);
		Mockito.when(targetMs.path(Mockito.anyString())).thenReturn(targetMs);
		Mockito.when(clientMs.target(Mockito.anyString())).thenReturn(targetMs);
		restClient.setClientMs(clientMs);

		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_REQUEST);
		JsonNode envioCorreoNode = JsonUtil.getNodeReference(Constantes.PATH_MSG_ENVIO_CORREO, root);
		((ObjectNode) envioCorreoNode).put("archivoAdjunto", "archivotest");
		notificacionServiceImpl.notificacionCorreoAdjunto(JsonUtil.getTrama(root), 100L);
	}

	@Test
	public void notificacionadjuntoErrorHttpBusTest() throws MicroserviceException {
		AuditoriaServiceImpl auditoriaServiceImpl = Mockito.mock(AuditoriaServiceImpl.class);
		aspect.setAuditoriaService(auditoriaServiceImpl);
		// RESPONSE NOTIFICACION
		Client clientBus = Mockito.mock(Client.class);
		WebTarget target = Mockito.mock(WebTarget.class);
		Builder builder = Mockito.mock(Builder.class);
		Response response = Mockito.mock(Response.class);
		Mockito.when(response.getStatus()).thenReturn(500);
		Mockito.when(response.readEntity(String.class)).thenReturn(NOTIFICACION_RESPONSE);
		Mockito.when(builder.post(Mockito.any(Entity.class))).thenReturn(response);
		Mockito.when(target.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builder);
		Mockito.when(target.path(Mockito.anyString())).thenReturn(target);
		Mockito.when(clientBus.target(Mockito.anyString())).thenReturn(target);
		restClient.setClientBus(clientBus);

		// RESPONSE GENERAR ARCHIVO
		Client clientMs = Mockito.mock(Client.class);
		WebTarget targetMs = Mockito.mock(WebTarget.class);
		Builder builderMs = Mockito.mock(Builder.class);
		Response responseMs = Mockito.mock(Response.class);
		Mockito.when(responseMs.getStatus()).thenReturn(200);
		Mockito.when(responseMs.readEntity(String.class)).thenReturn(GENERARARCHIVO_RESPONSE);
		Mockito.when(builderMs.post(Mockito.any(Entity.class))).thenReturn(responseMs);
		Mockito.when(targetMs.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builderMs);
		Mockito.when(targetMs.path(Mockito.anyString())).thenReturn(targetMs);
		Mockito.when(clientMs.target(Mockito.anyString())).thenReturn(targetMs);
		restClient.setClientMs(clientMs);

		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_REQUEST);
		JsonNode envioCorreoNode = JsonUtil.getNodeReference(Constantes.PATH_MSG_ENVIO_CORREO, root);
		((ObjectNode) envioCorreoNode).put("archivoAdjunto", "archivotest");
		notificacionServiceImpl.notificacionCorreoAdjunto(JsonUtil.getTrama(root), 100L);
	}

	@Test
	public void notificacionadjuntoTimeoutTest() throws MicroserviceException {
		AuditoriaServiceImpl auditoriaServiceImpl = Mockito.mock(AuditoriaServiceImpl.class);
		aspect.setAuditoriaService(auditoriaServiceImpl);
		// RESPONSE NOTIFICACION
		Client clientBus = Mockito.mock(Client.class);
		WebTarget target = Mockito.mock(WebTarget.class);
		Builder builder = Mockito.mock(Builder.class);
		Response response = Mockito.mock(Response.class);
		Mockito.when(response.getStatus()).thenReturn(200);
		Mockito.when(response.readEntity(String.class)).thenReturn(NOTIFICACION_TIMEOUT);
		Mockito.when(builder.post(Mockito.any(Entity.class))).thenReturn(response);
		Mockito.when(target.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builder);
		Mockito.when(target.path(Mockito.anyString())).thenReturn(target);
		Mockito.when(clientBus.target(Mockito.anyString())).thenReturn(target);
		restClient.setClientBus(clientBus);

		// RESPONSE GENERAR ARCHIVO
		Client clientMs = Mockito.mock(Client.class);
		WebTarget targetMs = Mockito.mock(WebTarget.class);
		Builder builderMs = Mockito.mock(Builder.class);
		Response responseMs = Mockito.mock(Response.class);
		Mockito.when(responseMs.getStatus()).thenReturn(200);
		Mockito.when(responseMs.readEntity(String.class)).thenReturn(GENERARARCHIVO_RESPONSE);
		Mockito.when(builderMs.post(Mockito.any(Entity.class))).thenReturn(responseMs);
		Mockito.when(targetMs.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builderMs);
		Mockito.when(targetMs.path(Mockito.anyString())).thenReturn(targetMs);
		Mockito.when(clientMs.target(Mockito.anyString())).thenReturn(targetMs);
		restClient.setClientMs(clientMs);

		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_REQUEST);
		JsonNode envioCorreoNode = JsonUtil.getNodeReference(Constantes.PATH_MSG_ENVIO_CORREO, root);
		((ObjectNode) envioCorreoNode).put("archivoAdjunto", "archivotest");
		notificacionServiceImpl.notificacionCorreo(JsonUtil.getTrama(root), 100L);
	}

	@SuppressWarnings("serial")
	@Test
	public void notificacionErrorIOExceptionTest() throws MicroserviceException, JsonProcessingException, IOException {
		ObjectMapper mapper = Mockito.mock(ObjectMapper.class);
		Mockito.when(mapper.readTree(Mockito.anyString())).thenThrow(new IOException("Junit IO exception") {
		});
		notificacionServiceImpl.setObjectMapper(mapper);
		notificacionServiceImpl.notificacionCorreoError(NOTIFICACION_REQUEST, 100L);
	}

	@SuppressWarnings("serial")
	@Test
	public void notificacionJsonProcessingExceptionTest() throws MicroserviceException, IOException {
		ObjectMapper mapper = Mockito.mock(ObjectMapper.class);
		Mockito.when(mapper.readTree(Mockito.anyString())).thenReturn(JsonUtil.getRootNode(NOTIFICACION_REQUEST));
		Mockito.when(mapper.writeValueAsString(Mockito.any(JsonNode.class)))
				.thenThrow(new JsonProcessingException("Junit JsonProcessingException") {
				});
		AuditoriaServiceImpl auditoriaServiceImpl = Mockito.mock(AuditoriaServiceImpl.class);
		aspect.setAuditoriaService(auditoriaServiceImpl);
		Client client = Mockito.mock(Client.class);
		WebTarget target = Mockito.mock(WebTarget.class);
		Builder builder = Mockito.mock(Builder.class);
		Response response = Mockito.mock(Response.class);
		Mockito.when(response.getStatus()).thenReturn(200);
		Mockito.when(response.readEntity(String.class)).thenReturn(NOTIFICACION_RESPONSE);
		Mockito.when(builder.post(Mockito.any(Entity.class))).thenReturn(response);
		Mockito.when(target.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builder);
		Mockito.when(target.path(Mockito.anyString())).thenReturn(target);
		Mockito.when(client.target(Mockito.anyString())).thenReturn(target);
		restClient.setClientBus(client);
		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_REQUEST);
		((ObjectNode) root).put("numeroReintentos", "1");
		notificacionServiceImpl.setObjectMapper(mapper);
		notificacionServiceImpl.notificacionCorreo(JsonUtil.getTrama(root), 100L);
	}

	@Test
	public void notificacionProcessingExceptionTest() throws MicroserviceException {
		AuditoriaServiceImpl auditoriaServiceImpl = Mockito.mock(AuditoriaServiceImpl.class);
		aspect.setAuditoriaService(auditoriaServiceImpl);
		Client client = Mockito.mock(Client.class);
		WebTarget target = Mockito.mock(WebTarget.class);
		Builder builder = Mockito.mock(Builder.class);
		Response response = Mockito.mock(Response.class);
		Mockito.when(response.getStatus()).thenReturn(200);
		Mockito.when(response.readEntity(String.class)).thenReturn(NOTIFICACION_RESPONSE);
		Mockito.when(builder.post(Mockito.any(Entity.class))).thenThrow(new ProcessingException("Error Junit"));
		Mockito.when(target.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builder);
		Mockito.when(target.path(Mockito.anyString())).thenReturn(target);
		Mockito.when(client.target(Mockito.anyString())).thenReturn(target);
		restClient.setClientBus(client);
		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_REQUEST);
		((ObjectNode) root).put("numeroReintentos", "1");
		notificacionServiceImpl.notificacionCorreo(JsonUtil.getTrama(root), 100L);
	}

	@Test
	public void notificacionRestClientExceptionTest() throws MicroserviceException {
		AuditoriaServiceImpl auditoriaServiceImpl = Mockito.mock(AuditoriaServiceImpl.class);
		aspect.setAuditoriaService(auditoriaServiceImpl);
		Client client = Mockito.mock(Client.class);
		WebTarget target = Mockito.mock(WebTarget.class);
		Builder builder = Mockito.mock(Builder.class);
		Response response = Mockito.mock(Response.class);
		Mockito.when(response.getStatus()).thenReturn(200);
		Mockito.when(response.readEntity(String.class)).thenReturn(NOTIFICACION_RESPONSE);
		Mockito.when(builder.post(Mockito.any(Entity.class))).thenThrow(new RestClientException("Error Junit"));
		Mockito.when(target.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builder);
		Mockito.when(target.path(Mockito.anyString())).thenReturn(target);
		Mockito.when(client.target(Mockito.anyString())).thenReturn(target);
		restClient.setClientBus(client);
		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_REQUEST);
		((ObjectNode) root).put("numeroReintentos", "1");
		notificacionServiceImpl.notificacionCorreo(JsonUtil.getTrama(root), 100L);
	}

	@Test
	public void prepararReintentoTest() {
		List<String> idFallidos = new ArrayList<>();
		idFallidos.add("1");
		notificacionServiceImpl.prepararReintento(idFallidos, NOTIFICACION_REQUEST, "JUNITTEST0001", 100L);
	}

}

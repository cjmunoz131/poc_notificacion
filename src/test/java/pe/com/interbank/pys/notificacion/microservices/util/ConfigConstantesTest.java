package pe.com.interbank.pys.notificacion.microservices.util;

import org.junit.Test;

public class ConfigConstantesTest {
	
	@Test(expected= IllegalAccessError.class)
	public void constructorConfigTest(){
		new ConfigConstantes();
	}
	
	@Test(expected= IllegalAccessError.class)
	public void constructorTest(){
		new Constantes();
	}

}

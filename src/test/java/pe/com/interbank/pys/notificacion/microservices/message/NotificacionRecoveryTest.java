package pe.com.interbank.pys.notificacion.microservices.message;

import static org.junit.Assert.assertNull;

import java.io.IOException;

import org.junit.Test;
import org.mockito.Mockito;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;

public class NotificacionRecoveryTest {
	
	private String NOTIFICACION_REQUEST = PropertiesCache.getInstance().getProperty("test.trama.notificacion.request");
	
	@Test
	public void generarRequestNotificacionIOExceptionTest() throws JsonProcessingException, IOException {
		ObjectMapper mapper = Mockito.mock(ObjectMapper.class);
		Mockito.when(mapper.readTree(Mockito.anyString())).thenThrow(new IOException("junit error IO"));
		NotificacionRecovery recovery = new NotificacionRecovery();
		recovery.setObjectMapper(mapper);
		assertNull(recovery.generarRequestNotificacion("notificacion", NOTIFICACION_REQUEST, 100L));
	}
	
	@SuppressWarnings("serial")
	@Test
	public void generarRequestNotificacionJsonExceptionTest() throws JsonProcessingException, IOException {
		ObjectMapper mapper = Mockito.mock(ObjectMapper.class);
		Mockito.when(mapper.readTree(Mockito.anyString())).thenThrow(new JsonProcessingException("junit error Json"){});
		NotificacionRecovery recovery = new NotificacionRecovery();
		recovery.setObjectMapper(mapper);
		assertNull(recovery.generarRequestNotificacion("notificacion", NOTIFICACION_REQUEST, 100L));
	}

}

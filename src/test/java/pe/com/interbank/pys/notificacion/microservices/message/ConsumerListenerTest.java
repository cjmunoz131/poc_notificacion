package pe.com.interbank.pys.notificacion.microservices.message;

import javax.ws.rs.core.Response;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import pe.com.interbank.pys.notificacion.microservices.server.NotificacionServer;
import pe.com.interbank.pys.notificacion.microservices.service.NotificacionServiceImpl;
import pe.com.interbank.pys.notificacion.microservices.service.RouterServiceImpl;
import pe.com.interbank.pys.notificacion.microservices.service.SecurityRestClient;
import pe.com.interbank.pys.notificacion.microservices.util.ConfigConstantes;
import pe.com.interbank.pys.trace.microservices.exceptions.GenericException;
import pe.com.interbank.pys.trace.microservices.exceptions.MicroserviceException;
import pe.com.interbank.pys.trace.microservices.util.Encryptor;
import pe.com.interbank.pys.trace.microservices.util.JsonUtil;
import pe.com.interbank.pys.trace.microservices.util.PropertiesCache;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { NotificacionServer.class })
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ConsumerListenerTest {

	private static final String NOTIFICACION_REQUEST = PropertiesCache.getInstance().getProperty("test.trama.notificacion.request");
	private static final String NOTIFICACION_ERROR_REQUEST = PropertiesCache.getInstance().getProperty("test.trama.notificacionerror.request");
	private static final String NOTIFICACION_RESPONSE = PropertiesCache.getInstance().getProperty("test.trama.notificacion.response");
	public static final String NOTIFICACION_REQUEST_SMS = PropertiesCache.getInstance().getProperty("test.trama.notificar.request");
	public static final String NOTIFICACION_RESPONSE_SMS = PropertiesCache.getInstance().getProperty("test.trama.notificar.response");
	//public static final String NOTIFICACION_REQUEST_SMS = "{\"operation\":\"notificar\",\"usuarioId\":\"1234567\",\"operador\":\"C\",\"nroCelular\":\"51950201191\",\"canal\":\"avi\",\"messageId\":\"ABP-20171113-0002\",\"mensajeSMS\":\"MENSAJE 1\"}";
	//public static final String NOTIFICACION_RESPONSE_SMS = "{\"MessageResponse\":{\"Header\":{\"HeaderResponse\":{\"timestamp\":\"2018-07-20T17:25:43.919620-05:00\",\"status\":{\"responseType\":\"0\",\"busResponseCode\":\"0\",\"busResponseMessage\":\"EJECUCION CON EXITO\",\"srvResponseCode\":\"0\",\"srvResponseMessage\":\"20180720172543836614d76b\",\"messageIdRes\":\"SGI_UAT_0100dec1fae0-2d19-415e-a3c4-9978c94cf161\"}}},\"Body\":{\"envioMensajeResponse\":{}}}}";
	
	@Autowired
	private NotificacionServiceImpl serviceImpl;

	@Autowired
	ConsumerListener consumerListener;

	@SuppressWarnings("unchecked")
	@Test
	public void onMessageTest() {
		SecurityRestClient securityRestClient = Mockito.mock(SecurityRestClient.class);
		Response response = Mockito.mock(Response.class);
		Mockito.when(response.getStatus()).thenReturn(200);
		Mockito.when(response.readEntity(String.class)).thenReturn(NOTIFICACION_RESPONSE);
		Mockito.when(securityRestClient.invokeExternalService(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean()))
				.thenReturn(response);
		serviceImpl.setSecurityRestClient(securityRestClient);
		
		ConsumerRecord<Integer, String> message = Mockito.mock(ConsumerRecord.class);
		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_REQUEST);
		((ObjectNode) root).put("operation", PropertiesCache.getInstance().getProperty(ConfigConstantes.SERVICE_ID));
		String value = Encryptor.encrypt(PropertiesCache.getInstance().getProperty(ConfigConstantes.AES_KEY),
				PropertiesCache.getInstance().getProperty(ConfigConstantes.AES_VECTOR), JsonUtil.getTrama(root));
		Mockito.when(message.value()).thenReturn(value);
		Mockito.when(message.topic())
				.thenReturn(PropertiesCache.getInstance().getProperty(ConfigConstantes.TOPICO_ASINCRONO));
		Mockito.when(message.offset()).thenReturn(100L);
		RouterServiceImpl routerService = new RouterServiceImpl();
		routerService.setNotificacionService(serviceImpl);
		consumerListener.setService(routerService);
		consumerListener.onMessage(message);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void onMessagRecoverTest() {
		SecurityRestClient securityRestClient = Mockito.mock(SecurityRestClient.class);
		Response response = Mockito.mock(Response.class);
		Mockito.when(response.getStatus()).thenReturn(200);
		Mockito.when(response.readEntity(String.class)).thenReturn(NOTIFICACION_RESPONSE);
		Mockito.when(securityRestClient.invokeExternalService(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean()))
				.thenReturn(response);
		serviceImpl.setSecurityRestClient(securityRestClient);
		ConsumerRecord<Integer, String> message = Mockito.mock(ConsumerRecord.class);
		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_ERROR_REQUEST);
		((ObjectNode) root).put("operation", "actualizarcampania");
		String value = Encryptor.encrypt(PropertiesCache.getInstance().getProperty(ConfigConstantes.AES_KEY),
				PropertiesCache.getInstance().getProperty(ConfigConstantes.AES_VECTOR), JsonUtil.getTrama(root));
		Mockito.when(message.value()).thenReturn(value);
		Mockito.when(message.topic())
				.thenReturn(PropertiesCache.getInstance().getProperty(ConfigConstantes.TOPICO_RECOVER));
		Mockito.when(message.offset()).thenReturn(100L);
		RouterServiceImpl routerService = new RouterServiceImpl();
		routerService.setNotificacionService(serviceImpl);
		consumerListener.setService(routerService);
		consumerListener.onMessage(message);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void onMessageAdjuntoTest() throws GenericException {
		ConsumerRecord<Integer, String> message = Mockito.mock(ConsumerRecord.class);
		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_REQUEST);
		((ObjectNode) root).put("operation", PropertiesCache.getInstance().getProperty(ConfigConstantes.SERVICE_NOTIFICACIONADJUNTO));
		String value = Encryptor.encrypt(PropertiesCache.getInstance().getProperty(ConfigConstantes.AES_KEY),
				PropertiesCache.getInstance().getProperty(ConfigConstantes.AES_VECTOR), JsonUtil.getTrama(root));
		Mockito.when(message.value()).thenReturn(value);
		Mockito.when(message.topic())
				.thenReturn(PropertiesCache.getInstance().getProperty(ConfigConstantes.TOPICO_ASINCRONO));
		Mockito.when(message.offset()).thenReturn(100L);
		RouterServiceImpl routerService = Mockito.mock(RouterServiceImpl.class);
		routerService.setNotificacionService(serviceImpl);
		consumerListener.setService(routerService);
		consumerListener.onMessage(message);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void onMessageAdjuntoExceptionTest() throws GenericException {
		NotificacionServiceImpl service = Mockito.mock(NotificacionServiceImpl.class);
		ConsumerRecord<Integer, String> message = Mockito.mock(ConsumerRecord.class);
		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_REQUEST);
		((ObjectNode) root).put("operation", PropertiesCache.getInstance().getProperty(ConfigConstantes.SERVICE_NOTIFICACIONADJUNTO));
		String value = Encryptor.encrypt(PropertiesCache.getInstance().getProperty(ConfigConstantes.AES_KEY),
				PropertiesCache.getInstance().getProperty(ConfigConstantes.AES_VECTOR), JsonUtil.getTrama(root));
		Mockito.when(message.value()).thenReturn(value);
		Mockito.when(message.topic())
				.thenReturn(PropertiesCache.getInstance().getProperty(ConfigConstantes.TOPICO_ASINCRONO));
		Mockito.when(message.offset()).thenReturn(100L);
		Mockito.when(service.notificacionCorreoAdjunto(JsonUtil.getTrama(root),100L)).thenThrow(new MicroserviceException("test junit adjunto"));
		RouterServiceImpl routerService = new RouterServiceImpl();
		routerService.setNotificacionService(serviceImpl);
		consumerListener.setService(routerService);
		consumerListener.onMessage(message);
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void onMessageSMSTest() {
		SecurityRestClient securityRestClient = Mockito.mock(SecurityRestClient.class);
		Response response = Mockito.mock(Response.class);
		Mockito.when(response.getStatus()).thenReturn(200);
		Mockito.when(response.readEntity(String.class)).thenReturn(NOTIFICACION_RESPONSE_SMS);
		Mockito.when(securityRestClient.invokeExternalService(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean()))
				.thenReturn(response);
		serviceImpl.setSecurityRestClient(securityRestClient);
		
		ConsumerRecord<Integer, String> message = Mockito.mock(ConsumerRecord.class);
		JsonNode root = JsonUtil.getRootNode(NOTIFICACION_REQUEST_SMS);
		((ObjectNode) root).put("operation", PropertiesCache.getInstance().getProperty(ConfigConstantes.SERVICE_ID_NOTIFICAR));
		String value = Encryptor.encrypt(PropertiesCache.getInstance().getProperty(ConfigConstantes.AES_KEY),
				PropertiesCache.getInstance().getProperty(ConfigConstantes.AES_VECTOR), JsonUtil.getTrama(root));
		Mockito.when(message.value()).thenReturn(value);
		Mockito.when(message.topic())
				.thenReturn(PropertiesCache.getInstance().getProperty(ConfigConstantes.TOPICO_ASINCRONO));
		Mockito.when(message.offset()).thenReturn(100L);
		RouterServiceImpl routerService = new RouterServiceImpl();
		routerService.setNotificacionService(serviceImpl);
		consumerListener.setService(routerService);
		consumerListener.onMessage(message);

	}

}

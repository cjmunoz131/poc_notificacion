package pe.com.interbank.pys.notificacion.microservices.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import pe.com.interbank.pys.notificacion.microservices.model.correo.ArchivoAdjunto;
import pe.com.interbank.pys.notificacion.microservices.model.correo.Attachment;
import pe.com.interbank.pys.notificacion.microservices.model.correo.BodyRequest;
import pe.com.interbank.pys.notificacion.microservices.model.correo.BodyResponse;
import pe.com.interbank.pys.notificacion.microservices.model.correo.GenerarArchivoOut;
import pe.com.interbank.pys.notificacion.microservices.model.correo.HeaderRequest;
import pe.com.interbank.pys.notificacion.microservices.model.correo.HeaderRequestType;
import pe.com.interbank.pys.notificacion.microservices.model.correo.Identity;
import pe.com.interbank.pys.notificacion.microservices.model.correo.MessageNotificacion;
import pe.com.interbank.pys.notificacion.microservices.model.correo.MessageNotificacionResponse;
import pe.com.interbank.pys.notificacion.microservices.model.correo.MessageNotificacionResponseType;
import pe.com.interbank.pys.notificacion.microservices.model.correo.MessageNotificacionType;
import pe.com.interbank.pys.notificacion.microservices.model.correo.MessageRequest;
import pe.com.interbank.pys.notificacion.microservices.model.correo.MessageResponse;
import pe.com.interbank.pys.notificacion.microservices.model.correo.NotificacionRequest;
import pe.com.interbank.pys.notificacion.microservices.model.correo.NotificacionRequestType;
import pe.com.interbank.pys.notificacion.microservices.model.correo.NotificacionResponse;
import pe.com.interbank.pys.notificacion.microservices.model.correo.NotificacionResponseType;
import pe.com.interbank.pys.notificacion.microservices.model.correo.Parameter;
import pe.com.interbank.pys.notificacion.microservices.model.correo.ParameterType;
import pe.com.interbank.pys.notificacion.microservices.model.correo.Request;

public class NotificacionModelTest {

	@Test
	public void notificacionRequestModelTest() {
		List<ParameterType> parameters = new ArrayList<>();
		Parameter parameter = new Parameter("key", "value");
		parameter.setKey(parameter.getKey());
		parameter.setValue(parameter.getValue());
		ParameterType parameterType = new ParameterType(parameter);
		parameterType.setParameter(parameterType.getParameter());
		parameters.add(parameterType);
		List<Attachment> attachments = new ArrayList<>();
		Attachment attachment = new Attachment("attachment");
		attachment.setAttachment(attachment.getAttachment());
		attachments.add(attachment);
		MessageNotificacion messageNotificacions = new MessageNotificacion("id", "from", "destination", "cc", "cco", "emailRespuesta", "type", "templateName", "tags", "carrier", attachments, "appSource", parameters);
		messageNotificacions.setAppSource(messageNotificacions.getAppSource());
		messageNotificacions.setAttachments(messageNotificacions.getAttachments());
		messageNotificacions.setCarrier(messageNotificacions.getCarrier());
		messageNotificacions.setCc(messageNotificacions.getCc());
		messageNotificacions.setCco(messageNotificacions.getCco());
		messageNotificacions.setDestination(messageNotificacions.getDestination());
		messageNotificacions.setEmailRespuesta(messageNotificacions.getEmailRespuesta());
		messageNotificacions.setFrom(messageNotificacions.getFrom());
		messageNotificacions.setId(messageNotificacions.getId());
		messageNotificacions.setParameters(messageNotificacions.getParameters());
		messageNotificacions.setTags(messageNotificacions.getTags());
		messageNotificacions.setTemplateName(messageNotificacions.getType());
		messageNotificacions.setType(messageNotificacions.getType());
		MessageNotificacionType messageNotificacionType = new MessageNotificacionType(messageNotificacions);
		messageNotificacionType.setMessageNotificacions(messageNotificacionType.getMessageNotificacions());
		List<MessageNotificacionType> messageNotificacionTypeList = new ArrayList<>();
		messageNotificacionTypeList.add(messageNotificacionType);
		NotificacionRequestType notificacionRequestType = new NotificacionRequestType("identity", "credential", messageNotificacionTypeList);
		notificacionRequestType.setCredential(notificacionRequestType.getCredential());
		notificacionRequestType.setIdentity(notificacionRequestType.getIdentity());
		notificacionRequestType.setMessageNotificacionType(notificacionRequestType.getMessageNotificacionType());
		BodyRequest body = new BodyRequest(notificacionRequestType);
		body.setNotificacionRequestType(notificacionRequestType);
		Identity identity = new Identity("netId", "userId", "supervisorId", "deviceId", "serverId", "branchCode");
		identity.setBranchCode(identity.getBranchCode());
		identity.setDeviceId(identity.getDeviceId());
		identity.setNetId(identity.getNetId());
		identity.setServerId(identity.getServerId());
		identity.setSupervisorId(identity.getSupervisorId());
		identity.setUserId(identity.getUserId());
		Request request = new Request("serviceId", "consumerId", "moduleId", "channelCode", "messageId", "timestamp", "countryCode", "groupMember", "referenceNumber");
		request.setChannelCode(request.getChannelCode());
		request.setConsumerId(request.getConsumerId());
		request.setCountryCode(request.getCountryCode());
		request.setGroupMember(request.getGroupMember());
		request.setMessageId(request.getMessageId());
		request.setModuleId(request.getModuleId());
		request.setReferenceNumber(request.getReferenceNumber());
		request.setServiceId(request.getServiceId());
		request.setTimestamp(request.getTimestamp());
		HeaderRequestType headerRequest = new HeaderRequestType(request, identity);
		headerRequest.setRequest(request);
		headerRequest.setIdentity(identity);
		HeaderRequest header = new HeaderRequest(headerRequest);
		header.setHeaderRequest(headerRequest);
		MessageRequest messageRequest = new MessageRequest(header, body);
		messageRequest.setHeader(header);
		messageRequest.setBody(body);
		NotificacionRequest notificacionRequest = new NotificacionRequest(messageRequest);
		notificacionRequest.setMessageRequest(notificacionRequest.getMessageRequest());
		assertNotNull(notificacionRequest);
		
	}
	
	@Test
	public void notificacionResponseModelTest() {
		List<MessageNotificacionResponse> messageNotificacionResponseList = new ArrayList<>();
		MessageNotificacionResponse messageNotificacionResponse = new MessageNotificacionResponse("id", "externalId", "statusId", "lastChange");
		messageNotificacionResponse.setExternalId(messageNotificacionResponse.getExternalId());
		messageNotificacionResponse.setId(messageNotificacionResponse.getId());
		messageNotificacionResponse.setLastChange(messageNotificacionResponse.getLastChange());
		messageNotificacionResponse.setStatusId(messageNotificacionResponse.getStatusId());
		messageNotificacionResponseList.add(messageNotificacionResponse);
		MessageNotificacionResponseType messageNotificacionResponseType = new MessageNotificacionResponseType(messageNotificacionResponseList);
		messageNotificacionResponseType.setMessageNotificacionResponse(messageNotificacionResponseType.getMessageNotificacionResponse());
		NotificacionResponseType notificacionResponseType = new NotificacionResponseType(messageNotificacionResponseType);
		notificacionResponseType.setMessageNotificacionResponseType(notificacionResponseType.getMessageNotificacionResponseType());
		BodyResponse body = new BodyResponse(notificacionResponseType);
		body.setNotificacionResponseType(body.getNotificacionResponseType());
		Status status = new Status("responseType", "busResponseCode", "busResponseMessage", "srvResponseCode", "srvResponseMessage", "eqvResponseCode", "eqvResponseMessage", "messageIdResBus");
		status.setBusResponseCode(status.getBusResponseCode());
		status.setBusResponseMessage(status.getBusResponseMessage());
		status.setEqvResponseCode(status.getEqvResponseCode());
		status.setEqvResponseMessage(status.getEqvResponseMessage());
		status.setMessageIdResBus(status.getMessageIdResBus());
		status.setResponseType(status.getResponseType());
		status.setSrvResponseCode(status.getSrvResponseCode());
		status.setSrvResponseMessage(status.getSrvResponseMessage());
		Response response = new Response("serviceId", "consumerId", "moduleId", "channelCode", "messageId", "timestamp", "countryCode", "groupMember", "referenceNumber");
		response.setChannelCode(response.getChannelCode());
		response.setConsumerId(response.getConsumerId());
		response.setCountryCode(response.getCountryCode());
		response.setGroupMember(response.getGroupMember());
		response.setMessageId(response.getMessageId());
		response.setModuleId(response.getModuleId());
		response.setReferenceNumber(response.getReferenceNumber());
		response.setServiceId(response.getServiceId());
		response.setTimestamp(response.getTimestamp());
		HeaderResponse headerResponse = new HeaderResponse(response, status);
		headerResponse.setResponse(headerResponse.getResponse());
		headerResponse.setStatus(headerResponse.getStatus());
		HeaderResponseType header = new HeaderResponseType(headerResponse);
		header.setHeaderResponse(header.getHeaderResponse());
		MessageResponse messageResponse = new MessageResponse(header, body);
		messageResponse.setHeader(header);
		messageResponse.setBody(body);
		NotificacionResponse notificacionResponse = new NotificacionResponse(messageResponse);
		notificacionResponse.setMessageResponse(notificacionResponse.getMessageResponse());
		assertNotNull(notificacionResponse);
		
	}
	
	@Test
	public void generarArchivoModeltest(){
		List<ParameterType> parameters = new ArrayList<>();
		ArchivoAdjunto adjunto = new ArchivoAdjunto("tipoArchivo", "template", "nombreArchivo", "numeroDocumento", parameters);
		adjunto.setNombreArchivo(adjunto.getNombreArchivo());
		adjunto.setNumeroDocumento(adjunto.getNumeroDocumento());
		adjunto.setParameters(adjunto.getParameters());
		adjunto.setTemplate(adjunto.getTemplate());
		adjunto.setTipoArchivo(adjunto.getTipoArchivo());
		assertNotNull(adjunto);
		
		GenerarArchivoOut archivoOut = new GenerarArchivoOut("nombreArchivo", "rutaArchivo", "tipoArchivo");
		GenerarArchivoOut generarArchivoOut = new GenerarArchivoOut();
		generarArchivoOut.setNombreArchivo(archivoOut.getNombreArchivo());
		generarArchivoOut.setRutaArchivo(archivoOut.getRutaArchivo());
		generarArchivoOut.setTipoArchivo(archivoOut.getTipoArchivo());
		
		assertNotNull(generarArchivoOut);
		
	}

}

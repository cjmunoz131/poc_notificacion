# **Interbank PYS - Microservicio NOTIFICACION**
# Microservicio que realiza la generación de archivos (ejem: Carta Cuenta Sueldo, Carta Dep. Plazo) y publicación en servidor SFTP.

## Descripción

Unificación de los Microservicios **IBK-PYS-NOTIFICACION-MS** y **IBK-PYS-NOTIFICAR-MS**.

- IBK-PYS-NOTIFICACION-MS

    > **NOTIFICACION :** Funcionalidad de enviar **correos** C/S Adjuntos a los clientes mediante Quantic.
     Para la funcionalida de envio de correo electronico con **ADJUNTO** es requisito el microservicio **IBK-PYS-GENERARARCHIVO-MS** , microservicio que genera o descarga reportes.

- IBK-PYS-NOTIFICAR-MS

    > **NOTIFICAR :** Funcionalidad de enviar **SMS** a los clientes.

#### Temporal build artifact (.jar) till resolve Tests

```sh
$ mvn clean install -o -Dmaven.test.skip=true 
```
## DEV
### ConfigMap 

#### Create
```sh
$ kubectl create configmap ibk-pys-notificacion-ms-properties --from-file=config-dev.properties
```
#### Replace 
```sh
$ kubectl create configmap ibk-pys-notificacion-ms-properties --dry-run --from-file=config-dev.properties -o yaml | kubectl replace -f -
```

### Build Azure with Tag

```sh
$ docker build -t="acreu2c003cudidev01.azurecr.io/ibk-pys-notificacion-ms:0.0.2" --build-arg artifact_id=ibk-pys-notificacion-ms --build-arg artifact_version=0.0.2 .
```
### Push Azure Container Registry
```sh
$ docker push acreu2c003cudidev01.azurecr.io/ibk-pys-notificacion-ms:0.0.2
```

## UAT
### ConfigMap 

#### Create
```sh
$ kubectl create configmap ibk-pys-notificacion-ms-properties --from-file=config-uat.properties
```
#### Replace 
```sh
$ kubectl create configmap ibk-pys-notificacion-ms-properties --dry-run --from-file=config-uat.properties -o yaml | kubectl replace -f -
```

### Build Azure with Tag

```sh
$ docker build -t="acreu2c003cudiuat01.azurecr.io/ibk-pys-notificacion-ms:0.0.2" --build-arg artifact_id=ibk-pys-notificacion-ms --build-arg artifact_version=0.0.2 .
```
### Push Azure Container Registry
```sh
$ docker push acreu2c003cudiuat01.azurecr.io/ibk-pys-notificacion-ms:0.0.2
```


    
    

